# VONE-Sim
```
Repositorio dedicado a la creación de un simulador para el problema VONE Transparente elaborado en 
Java para ayudar en la investigación del mismo.

                Proyecto de Tesis de Grado  - Ingeniería en Informática
                Facultad Politécnica - Universidad Nacional de Asunción
```
### Integrantes
```
1. Univ. Alcides Rivarola

2. Univ. Fernando Saucedo
```