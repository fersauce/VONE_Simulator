package py.una.pol.vone.simulator.DAO;

import java.util.List;
import javax.persistence.EntityManager;

/**
 * Interface que representa el DAO del proyecto.
 * @author Fernando Saucedo <carlifer.fernando@gmail.com>
 * @param <E> Clase que representa la entidad
 * @param <K> Clase que representa el tipo de dato del PK
 * @version 1.0
 * @since 2017-03-11
 */
public interface GenericDAO<E, K> {
    public void agregar(E nuevo, EntityManager entity);
    public void actualizar(E actualizado, EntityManager entity);
    public void borrar(E borrado, EntityManager entity);
    public E buscar(K key, EntityManager entity);
    public List<E> getAll(EntityManager entity);
}
