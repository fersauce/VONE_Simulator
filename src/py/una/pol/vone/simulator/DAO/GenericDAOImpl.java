package py.una.pol.vone.simulator.DAO;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * Clase abstracta que implementa el DAO Generico.
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @param <E> Clase que representa la entidad
 * @param <K> Clase que representa el tipo de dato del PK
 * @version 1.0, 3/11/17
 */
public abstract class GenericDAOImpl<E extends Serializable, K extends Serializable>
        implements GenericDAO<E, K> {

    protected Class<E> entityClass;

    /**
     * Constructor de la clase.
     *
     */
    public GenericDAOImpl() {
        super();
        Type t = this.getClass().getGenericSuperclass();
        ParameterizedType genericSuperClass = (ParameterizedType) t;
        this.entityClass = (Class<E>) genericSuperClass.
                getActualTypeArguments()[0];
    }

    public Class<E> getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public void agregar(E nuevo, EntityManager entityManager) {
        entityManager.persist(nuevo);
    }

    @Override
    public E buscar(K key, EntityManager entityManager) {
        return entityManager.find(entityClass, key);
    }

    @Override
    public void actualizar(E actualizado, EntityManager entityManager) {
        entityManager.merge(actualizado);
    }

    @Override
    public void borrar(E borrado, EntityManager entityManager) {
        borrado = entityManager.merge(borrado);
        entityManager.remove(borrado);
    }

    @Override
    public List<E> getAll(EntityManager entityManager) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> cq = cb.createQuery(this.entityClass);
        Root<E> rootEntry = cq.from(this.entityClass);
        CriteriaQuery<E> all = cq.select(rootEntry);
        TypedQuery<E> allQuery = entityManager.createQuery(all);
        return (List<E>)allQuery.getResultList();
    }
}
