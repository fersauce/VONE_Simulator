/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.vone.simulator.DAO;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import py.una.pol.vone.simulator.entitys.SustrateNetworkEntity;
import py.una.pol.vone.simulator.model.SustrateNetwork;
import py.una.pol.vone.simulator.model.SustrateNode;
import py.una.pol.vone.simulator.model.VirtualNetwork;

/**
 *
 * @author Fernando Saucedo <carlifer.fernando@gmail.com>
 */
public class Prueba {

    @SuppressWarnings("CallToPrintStackTrace")
    public void pruebaCargaRed(SustrateNetwork redFisica,
            ArrayList<VirtualNetwork> redVirtual) {
        /*Se llama a los managers*/
        EntityManagerFactory entityManagerFactory = Persistence.
                createEntityManagerFactory("VONESimulatorPU");
        EntityManager entityManager = entityManagerFactory.
                createEntityManager();
        /*Se llama a los DAOS*/
        SustrateNetworkDAO sustrateNetworkDAO = new SustrateNetworkDAO();
        SustrateNodeDAO sustrateNodeDAO = new SustrateNodeDAO();
        SustrateEdgeDAO sustrateEdgeDAO = new SustrateEdgeDAO();
        VirtualNetworkDAO virtualNetworkDAO = new VirtualNetworkDAO();
        VirtualNodeDAO virtualNodeDAO = new VirtualNodeDAO();
        VirtualEdgeDAO virtualEdgeDAO = new VirtualEdgeDAO();

        try {
            entityManager.getTransaction().begin();
            SustrateNetworkEntity entidadRedSustrato = new SustrateNetworkEntity();
            for (SustrateNode nodoSustrato : redFisica.getNodosFisicos()) {

            }
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory emfactory = Persistence
                .createEntityManagerFactory("VONE_SimulatorPU");
        EntityManager entityManager = emfactory.
                createEntityManager();
        SustrateNetworkDAO sustrateNetworkDAO = new SustrateNetworkDAO();
        SustrateNodeDAO sustrateNodeDAO = new SustrateNodeDAO();
        SustrateEdgeDAO sustrateEdgeDAO = new SustrateEdgeDAO();
        VirtualNetworkDAO virtualNetworkDAO = new VirtualNetworkDAO();
        VirtualNodeDAO virtualNodeDAO = new VirtualNodeDAO();
        VirtualEdgeDAO virtualEdgeDAO = new VirtualEdgeDAO();
    }

}
