package py.una.pol.vone.simulator.DAO;

import py.una.pol.vone.simulator.entitys.SustrateEdgeEntity;

/**
 * DAO del Entity SustrateEdge
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 */
public class SustrateEdgeDAO extends GenericDAOImpl<SustrateEdgeEntity, Long>{

    public SustrateEdgeDAO() {
        super();
    }
}
