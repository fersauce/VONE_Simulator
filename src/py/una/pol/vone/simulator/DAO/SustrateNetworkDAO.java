package py.una.pol.vone.simulator.DAO;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import py.una.pol.vone.simulator.entitys.SustrateEdgeEntity;
import py.una.pol.vone.simulator.entitys.SustrateNetworkEntity;
import py.una.pol.vone.simulator.entitys.SustrateNodeEntity;

/**
 * DAO de SustrateNetworkEntity
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 */
public class SustrateNetworkDAO extends GenericDAOImpl<SustrateNetworkEntity, Long> {

    public SustrateNetworkDAO() {
        super();
    }

    /**
     * Metodo que devolvera la lista de nodos relacionados a una red fisica.
     *
     * @param red objeto que tiene informacion de la red de la cual se va a
     * buscar los nodos relacionados.
     * @param entityManager instancia para poder conectarse a la BD.
     * @return listado de nodos sustratos que cumple con las condiciones
     */
    public ArrayList<SustrateNodeEntity>
            nodosPorRedFisica(SustrateNetworkEntity red,
                    EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT f FROM "
                + "SustrateNodeEntity f WHERE f.redSustrato = :red").
                setParameter("red", red);
        return new ArrayList<>(query.getResultList());
    }

    /**
     * Metodo que devolvera la lista de enlaces relacionados a una red fisica.
     *
     * @param red objeto que tiene informacion de la red de la cual se va a
     * buscar los nodos relacionados.
     * @param entityManager instancia para poder conectarse a la BD.
     * @return listado de enlaces sustratos que cumple con las condiciones
     */
    public ArrayList<SustrateEdgeEntity>
            enlacesPorRedFisica(SustrateNetworkEntity red,
                    EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT f FROM "
                + "SustrateEdgeEntity f WHERE f.redSustrato = :red").
                setParameter("red", red);
        return new ArrayList<>(query.getResultList());
    }
}
