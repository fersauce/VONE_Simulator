package py.una.pol.vone.simulator.DAO;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import py.una.pol.vone.simulator.entitys.SustrateNetworkEntity;
import py.una.pol.vone.simulator.entitys.SustrateNodeEntity;

/**
 * DAO del Entity SustrateNode
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/12/17
 */
public class SustrateNodeDAO extends GenericDAOImpl<SustrateNodeEntity, Long> {

    public SustrateNodeDAO() {
        super();
    }

    /**
     * Metodo para obtener un nodo fisico por su identificador de red (no
     * confundir con PK de la BD).
     *
     * @param id entero que indica el identificador del nodo a obtener.
     * @param red objeto que tiene informacion para indicar la busqueda del
     * identificador dentro de los nodos que solo estan relacionados a esa red.
     * @param entityManager instancia para poder conectarse a la BD.
     * @return nodo sustrato que cumple con las condiciones.
     */
    public SustrateNodeEntity obtenerNodoPorIdentificador(int id,
            SustrateNetworkEntity red, EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT f FROM "
                + "SustrateNodeEntity f WHERE f.redSustrato = :red AND "
                + "f.identificador =:id").setParameter("red", red).
                setParameter("id", id);

        return (SustrateNodeEntity) query.getResultList().get(0);
    }
}
