package py.una.pol.vone.simulator.DAO;

import py.una.pol.vone.simulator.entitys.VirtualEdgeEntity;

/**
 * DAO del Entity VirtualEdge
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 */
public class VirtualEdgeDAO extends GenericDAOImpl<VirtualEdgeEntity, Long>{

    public VirtualEdgeDAO() {
        super();
    }
}
