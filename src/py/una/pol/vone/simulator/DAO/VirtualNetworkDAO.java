package py.una.pol.vone.simulator.DAO;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import py.una.pol.vone.simulator.entitys.VirtualEdgeEntity;
import py.una.pol.vone.simulator.entitys.VirtualNetworkEntity;
import py.una.pol.vone.simulator.entitys.VirtualNodeEntity;

/**
 * DAO del Entity VirtualNetwork
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 */
public class VirtualNetworkDAO extends GenericDAOImpl<VirtualNetworkEntity, Long>{

    public VirtualNetworkDAO() {
        super();
    }
    
    /**
     * Metodo que devolvera la lista de nodos relacionados a una red virtual.
     *
     * @param red objeto que tiene informacion de la red de la cual se va a
     * buscar los nodos relacionados.
     * @param entityManager instancia para poder conectarse a la BD.
     * @return listado de nodos sustratos que cumple con las condiciones
     */
    public ArrayList<VirtualNodeEntity> 
        nodosPorRedVirtual(VirtualNetworkEntity red, 
                EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT f FROM "
                + "VirtualNodeEntity f WHERE f.redVirtual = :red").
                setParameter("red", red);
        return new ArrayList<>(query.getResultList());
    }

    /**
     * Metodo que devolvera la lista de enlaces relacionados a una red virtual.
     *
     * @param red objeto que tiene informacion de la red de la cual se va a
     * buscar los nodos relacionados.
     * @param entityManager instancia para poder conectarse a la BD.
     * @return listado de nodos sustratos que cumple con las condiciones
     */
    public ArrayList<VirtualEdgeEntity> 
        enlacesPorRedVirtual(VirtualNetworkEntity red, 
                EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT f FROM "
                + "VirtualEdgeEntity f WHERE f.redVirtual = :red").
                setParameter("red", red);
        return new ArrayList<>(query.getResultList());
    }
}
