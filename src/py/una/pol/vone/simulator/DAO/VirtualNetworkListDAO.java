package py.una.pol.vone.simulator.DAO;

import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import py.una.pol.vone.simulator.entitys.VirtualNetworkEntity;
import py.una.pol.vone.simulator.entitys.VirtualNetworkListEntity;

/**
 * DAO del entity VirtualNetworkList
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 */
public class VirtualNetworkListDAO extends GenericDAOImpl<VirtualNetworkListEntity, Long> {

    public VirtualNetworkListDAO() {
        super();
    }

    /**
     * Metodo que devolvera la lista de conjuntos de redes virtuales estaticas o
     * dinamicas.
     *
     * @param esEstatico boolean que indica si quiere todas las redes
     * estaticas(true) o dinamicas(false).
     * @param entityManager instancia para poder conectarse a la BD.
     * @return listado de conjuntos de redes virtuales que cumple con las
     * condiciones.
     */
    public ArrayList<VirtualNetworkListEntity>
            obtenerRedesVirtualesPorTipo(boolean esEstatico,
                    EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT f FROM "
                + "VirtualNetworkListEntity f WHERE f.esEstatico "
                + "= :esEstatico").setParameter("esEstatico", esEstatico);
        return new ArrayList<>(query.getResultList());
    }

    /**
     * Metodo que retorna todas las redes que pertenecen al conjunto pasado.
     *
     * @param conjunto informacion del conjunto del cual se va a obtener todas
     * las redes virtuales asociadas a ella.
     * @param entityManager instancia para poder conectarse a la BD.
     * @return listado de redes virtuales que cumplen con la condicion.
     */
    public ArrayList<VirtualNetworkEntity>
            obtenerRedesAsociadas(VirtualNetworkListEntity conjunto,
                    EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT f FROM "
                + "VirtualNetworkEntity f WHERE f.lista = :set").
                setParameter("set", conjunto);
        return new ArrayList<>(query.getResultList());
    }
}
