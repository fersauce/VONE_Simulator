package py.una.pol.vone.simulator.DAO;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import py.una.pol.vone.simulator.entitys.VirtualNetworkEntity;
import py.una.pol.vone.simulator.entitys.VirtualNodeEntity;

/**
 * DAO del Entity VirtualNode
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/11/17
 */
public class VirtualNodeDAO extends GenericDAOImpl<VirtualNodeEntity, Long> {

    public VirtualNodeDAO() {
        super();
    }

    /**
     * Metodo para obtener un nodo virtual por su identificador de red (no
     * confundir con PK de la BD).
     *
     * @param id entero que indica el identificador del nodo a obtener.
     * @param red que tiene informacion para indicar la busqueda del
     * identificador dentro de los nodos que solo estan relacionados a esa red.
     * @param entityManager instancia para poder conectarse a la BD.
     * @return nodo virtual que cumple con las condiciones.
     */
    public VirtualNodeEntity obtenerNodoPorIdentificador(int id,
            VirtualNetworkEntity red, EntityManager entityManager) {
        Query query = entityManager.createQuery("SELECT f FROM "
                + "VirtualNodeEntity f WHERE f.redVirtual = :red AND "
                + "f.identificador = :id").setParameter("red", red).
                setParameter("id", id);
        return (VirtualNodeEntity) query.getResultList().get(0);
    }
}
