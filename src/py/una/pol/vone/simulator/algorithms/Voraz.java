package py.una.pol.vone.simulator.algorithms;

import edu.ufl.cise.bsmock.graph.Graph;
import edu.ufl.cise.bsmock.graph.ksp.Yen;
import edu.ufl.cise.bsmock.graph.util.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import py.una.pol.vone.simulator.model.Camino;
import py.una.pol.vone.simulator.model.SustrateEdge;
import py.una.pol.vone.simulator.model.SustrateNetwork;
import py.una.pol.vone.simulator.model.SustrateNode;
import py.una.pol.vone.simulator.model.VirtualEdge;
import py.una.pol.vone.simulator.model.VirtualNetwork;
import py.una.pol.vone.simulator.model.VirtualNode;
import static py.una.pol.vone.simulator.util.Simulations.asignarEnlacesVirtualesAFisicos;

/**
 * Clase en la cual los investigadores pueden agregar sus respectivos algoritmos
 * (a modo de ejemplo, en donde quieran agregar sus algoritmos queda a criterio
 * de cada uno).
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/28/17
 */
public class Voraz {

    /**
     * Metodo que realiza la asignacion de las redes virtuales sobre la red
     * fisica seleccionada de manera voraz para el problema dinamico.
     *
     * @param redFisica objeto con informacion del estado de la red fisica.
     * @param redVirtual objeto con la informacion de la red virtual a ser
     * mapeada dentro de la red fisica.
     * @return entero que indica si termino bien o mal.
     * @throws java.lang.Exception
     */
    public static int vorazDinamico(SustrateNetwork redFisica,
            VirtualNetwork redVirtual) throws Exception {
        /* Primero se ordena los nodos fisicos de mayor a menor en relacion 
         * a su capacidad actual y los nodos virtuales (tambien de mayor a 
         * menor)con respecto a la capacidad de CPU que requieren.
         */
        Collections.sort(redFisica.getNodosFisicos(),
                new Comparator<SustrateNode>() {
            @Override
            public int compare(SustrateNode o1, SustrateNode o2) {
                return new Integer(o2.capacidadActual()).
                        compareTo(o1.capacidadActual());
            }
        });
        Collections.sort(redVirtual.getNodosVirtuales(),
                new Comparator<VirtualNode>() {
            @Override
            public int compare(VirtualNode o1, VirtualNode o2) {
                return new Integer(o2.getCapacidadCPU()).
                        compareTo(o1.getCapacidadCPU());
            }
        });
        boolean[] asignado = new boolean[redFisica.getNodosFisicos().size()];
        for (int i = 0; i < redFisica.getNodosFisicos().size(); i++) {
            asignado[i] = false;
        }
        for (VirtualNode nodoVirtual : redVirtual.getNodosVirtuales()) {
            int posicion = 0;
            boolean encontrado = false;
            do {
                if (!asignado[posicion]) {
                    if (redFisica.getNodosFisicos().get(posicion).
                            tieneCapacidad(nodoVirtual.getCapacidadCPU())) {
                        redFisica.getNodosFisicos().get(posicion).
                                asignarRecursoCPU(nodoVirtual.
                                        getCapacidadCPU());
                        nodoVirtual.setNodoFisico((SustrateNode) redFisica.
                                getNodosFisicos().get(posicion));
                        nodoVirtual.setMapeado(true);
                        asignado[posicion] = true;
                        encontrado = true;
                    } else {
                        actualizarRedVirtual(redVirtual);
                        return 1;
                    }
                }
                posicion++;
            } while (!encontrado && posicion < redFisica.getNodosFisicos().
                    size());
            if (!encontrado) {
                actualizarRedVirtual(redVirtual);
                return 1;
            }
        }
        /*
         * Si llega a este punto, esto quiere decir que han sido asignados 
         * todos los nodos.
         */
        ArrayList<Camino> caminosPosibles
                = hallarKCaminos(redFisica, 3);
        for (VirtualEdge enlaceVirtual : redVirtual.getEnlacesVirtuales()) {
            Camino caminoSeleccionado = null;
            for (Camino camino : caminosPosibles) {
                if (camino.getOrigen() == enlaceVirtual.getNodoUno().
                        getNodoFisico().getIdentificador()) {
                    if (camino.getDestino() == enlaceVirtual.getNodoDos().
                            getNodoFisico().getIdentificador()) {
                        caminoSeleccionado = camino;
                        break;
                    }
                } else if (camino.getDestino() == enlaceVirtual.
                        getNodoUno().getNodoFisico().getIdentificador()) {
                    if (camino.getOrigen() == enlaceVirtual.getNodoDos().
                            getNodoFisico().getIdentificador()) {
                        caminoSeleccionado = camino;
                        break;
                    }
                }
            }
            int j = 0;
            boolean enlaceAsignado = false;
            do {
                ArrayList<SustrateEdge> enlacesFisicosARelacionar
                        = pasarEnlacesFisicos(caminoSeleccionado.getCaminos(),
                                redFisica.getEnlacesFisicos(), j);
                for (int i = 0; i <= redFisica.getCantidadFS()
                        - enlaceVirtual.getCantidadFS(); i++) {
                    if (estaLibre(enlacesFisicosARelacionar, i,
                            i + enlaceVirtual.getCantidadFS() - 1)) {
                        asignarFSEnlacesFisicos(enlacesFisicosARelacionar, i,
                                i + enlaceVirtual.getCantidadFS() - 1);
                        enlaceVirtual.setEnlaceFisico(
                                enlacesFisicosARelacionar);
                        enlaceVirtual.setPosicionFisica(i);
                        enlaceVirtual.setMapeado(true);
                        enlaceAsignado = true;
                        break;
                    }
                }
                j++;
            } while (!enlaceAsignado && j < 3);
            if (!enlaceAsignado) {
                actualizarRedVirtual(redVirtual);
                return 2;
            }
        }
        redVirtual.setMapeado(true);
        return 0;
    }

    /**
     * Metodo para ejecutar el problema estatico.
     *
     * @param redFisica
     * @param redesVirtuales
     * @throws java.lang.Exception
     */
    public static void vorazEstatico(SustrateNetwork redFisica,
            ArrayList<VirtualNetwork> redesVirtuales) throws Exception {
        /*Se ordena los requerimientos en orden decreciente de total de CPU.*/
        Collections.sort(redesVirtuales, new Comparator<VirtualNetwork>() {
            @Override
            public int compare(VirtualNetwork o1, VirtualNetwork o2) {
                return new Integer(o2.getTotalCPU()).
                        compareTo(o1.getTotalCPU());
            }
        });
        /*Ppr cada requerimiento, ejecutar el metodo dinamico.*/
        for (VirtualNetwork redVirtual : redesVirtuales) {
            redVirtual.setBanderaBloqueo(vorazDinamico((SustrateNetwork) redFisica.clone(), redVirtual));
            if (redVirtual.isMapeado()) {
                actualizarRedFisica(redFisica, redVirtual);
            } else {
                actualizarRedVirtual(redVirtual);
            }
        }
    }

    /**
     * Metodo para asignar recursos de CPU.
     *
     * @param nodoFisico nodo fisico al cual se asignara capacidad de CPU.
     * @param capacidad capacidad a ser asignada.
     */
    public static void asignarRecursoCPU(SustrateNode nodoFisico,
            int capacidad) {
        for (int i = 0; i < capacidad; i++) {
            nodoFisico.getCPU().add(i);
        }
    }

    /**
     * Metodo que utilizaremos para hallar los K caminos entre el par de nodos
     * seleccionado de una red, que en este caso sera montado como un grafo y en
     * base a la red fisica pasada como parametro.
     *
     * @param redFisica red fisica utilizada.
     * @param cantCaminos cantidad de caminos a hallar entre nodos.
     * @return Lista con los K caminos mas cortos entre todos los nodos del
     * grafo
     */
    public static ArrayList<Camino> hallarKCaminos(SustrateNetwork redFisica,
            int cantCaminos) {
        Graph grafo = new Graph();
        redFisica.getEnlacesFisicos().forEach((enlaceFisico) -> {
            grafo.addEdge(String.valueOf(enlaceFisico.getNodoUno().
                    getIdentificador()), String.valueOf(enlaceFisico.
                            getNodoDos().getIdentificador()),
                    Double.parseDouble("1"));
            grafo.addEdge(String.valueOf(enlaceFisico.getNodoDos().
                    getIdentificador()), String.valueOf(enlaceFisico.
                            getNodoUno().getIdentificador()),
                    Double.parseDouble("1"));
        });
        int cantidadNodos = grafo.numNodes();
        ArrayList<Camino> todosLosCaminos = new ArrayList<>();
        for (int origen = 0; origen < cantidadNodos; origen++) {
            for (int destino = origen + 1; destino < cantidadNodos; destino++) {
                /*Aqui se halla el camino entre origen y destino.*/
                List<Path> caminos;
                Yen algoritmoYen = new Yen();
                caminos = algoritmoYen.ksp(grafo, String.valueOf(origen),
                        Integer.toString(destino), cantCaminos);
                /*Se crea el camino entre ambos*/
                Camino caminoNuevo = new Camino();
                caminoNuevo.setOrigen(origen);
                caminoNuevo.setDestino(destino);
                caminoNuevo.setCaminos(caminos);
                /*Se agrega los K caminos encontrados al listado.*/
                todosLosCaminos.add(caminoNuevo);
            }
        }
        return todosLosCaminos;
    }

    /**
     * Metodo para convertir el path seleccionado en enlaces virtuales.
     *
     * @param camino listado de todos los caminos que conforman el enlace.
     * @param enlacesFisicos listado de todas los enlaces fisicos a que
     * pertenecen la red.
     * @param caminoElegido
     * @return lista de enlaces fisicos que corresponden al path pasado.
     */
    public static ArrayList<SustrateEdge>
            pasarEnlacesFisicos(List<Path> camino,
                    ArrayList<SustrateEdge> enlacesFisicos, int caminoElegido) {
        ArrayList<SustrateEdge> enlaces = new ArrayList<>();
        camino.get(caminoElegido).getEdges().forEach((enlaceDelCamino) -> {
            int idNodoUno = Integer.parseInt(enlaceDelCamino.getFromNode());
            int idNodoDos = Integer.parseInt(enlaceDelCamino.getToNode());
            for (SustrateEdge enlaceFisico : enlacesFisicos) {
                if (enlaceFisico.getNodoUno().getIdentificador() == idNodoUno) {
                    if (enlaceFisico.getNodoDos().getIdentificador()
                            == idNodoDos) {
                        enlaces.add(enlaceFisico);
                        break;
                    }
                } else if (enlaceFisico.getNodoDos().getIdentificador()
                        == idNodoUno) {
                    if (enlaceFisico.getNodoUno().getIdentificador()
                            == idNodoDos) {
                        enlaces.add(enlaceFisico);
                        break;
                    }
                }
            }
        });
        return enlaces;
    }

    /**
     * Metodo para verificar si el conjunto de enlace cuenta con la capacidad
     * necesaria para albergar al ligthpath o camino de la red virtual a mapear
     * sobre ellas.
     *
     * @param enlacesFisicos conjunto de enlaces fisicos a ser verificados.
     * @param posicionInicial posicion inicial del FS a verificar.
     * @param posicionFinal posicion final del FS a verificar.
     * @return true si esta libre ese camino.
     */
    public static boolean estaLibre(ArrayList<SustrateEdge> enlacesFisicos,
            int posicionInicial, int posicionFinal) {
        for (SustrateEdge enlace : enlacesFisicos) {
            for (int i = posicionInicial; i <= posicionFinal; i++) {
                if (enlace.devolverEstadoFS(i)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Metodo para asignar los FS de los enlaces fisicos seleccionados para
     * mapear el enlace virtual (se asume que antes ya se realizo la
     * verificacion de si estan libres dichas posiciones de FS y que son las
     * posiciones correctas.
     *
     * @param enlacesFisicos conjunto de enlaces fisicos a ser mapeados.
     * @param posicionInicial posicion inicial del FS a marcar como utilizado.
     * @param posicionFinal posicion final del FS a marcar como utilizado.
     */
    public static void asignarFSEnlacesFisicos(
            ArrayList<SustrateEdge> enlacesFisicos,
            int posicionInicial, int posicionFinal) {
        enlacesFisicos.forEach((enlace) -> {
            for (int i = posicionInicial; i <= posicionFinal; i++) {
                enlace.asignarEstadoFS(true, i);
            }
        });
    }

    /**
     * Metodo para actualizar el estado de la red fisica y actualizar las
     * referencias de la red virtual a objetos de la red fisica original.
     *
     * @param redFisica red fisica que va a ser actualizada.
     * @param redVirtual red virtual que va a ser actualizada sus referencias a
     * los objetos de la red fisica original.
     */
    public static void actualizarRedFisica(SustrateNetwork redFisica,
            VirtualNetwork redVirtual) {
        /*Primero se realiza la actualizacion para los nodos.*/
        redVirtual.getNodosVirtuales().forEach((nodoVirtual) -> {
            for (int i = 0; i < redFisica.getNodosFisicos().size(); i++) {
                if (redFisica.getNodosFisicos().get(i).getIdentificador()
                        == nodoVirtual.getNodoFisico().getIdentificador()) {
                    asignarRecursoCPU(redFisica.getNodosFisicos().get(i),
                            nodoVirtual.getCapacidadCPU());
                    nodoVirtual.setNodoFisico(redFisica.getNodosFisicos().
                            get(i));
                    break;
                }
            }
        });

        /*Siguiendo la ejecucion se realiza la actualizacion para los enlaces.*/
        redVirtual.getEnlacesVirtuales().forEach((enlaceVirtual) -> {
            int cantidadEnlaces = enlaceVirtual.getEnlaceFisico().size();
            for (int i = 0; i < cantidadEnlaces; i++) {
                for (int j = 0; j < redFisica.getEnlacesFisicos().size(); j++) {
                    if (redFisica.getEnlacesFisicos().get(j).getNombre().
                            equals(enlaceVirtual.getEnlaceFisico().get(i).
                                    getNombre())) {
                        asignarEnlacesVirtualesAFisicos(redFisica.
                                getEnlacesFisicos().get(j),
                                enlaceVirtual.getPosicionFisica(),
                                enlaceVirtual.getCantidadFS());
                        enlaceVirtual.getEnlaceFisico().
                                set(i, redFisica.getEnlacesFisicos().get(j));
                        break;
                    }
                }
            }
        });
    }

    /**
     * Metodo para limpiar los valores una red virtual que no fue mapeada.
     *
     * @param redVirtual red virtual la cual sera limpiada los valores de mapeo
     * que haya quedado.
     */
    public static void actualizarRedVirtual(VirtualNetwork redVirtual) {
        redVirtual.getNodosVirtuales().forEach((nodoVirtual) -> {
            nodoVirtual.setNodoFisico(null);
            nodoVirtual.setMapeado(false);
        });
        redVirtual.getEnlacesVirtuales().forEach((enlaceVirtual) -> {
            enlaceVirtual.setEnlaceFisico(new ArrayList<>());
            enlaceVirtual.setMapeado(false);
        });
    }
}
