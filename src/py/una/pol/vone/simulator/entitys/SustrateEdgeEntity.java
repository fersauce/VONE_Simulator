package py.una.pol.vone.simulator.entitys;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Entity que representa al enlace sustrato
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/16/17
 */
@Entity
public class SustrateEdgeEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idSustrateEdgeEntity;
    private int identificador;
    private String nombre;
    private double distancia;
    private int cantidadFS;
    @ManyToOne
    private SustrateNodeEntity nodoUno;
    @ManyToOne
    private SustrateNodeEntity nodoDos;
    @ManyToOne
    private SustrateNetworkEntity redSustrato;

    public SustrateEdgeEntity() {
        super();
    }

    public SustrateEdgeEntity(String nombre, float distancia, int cantidadFS) {
        super();
        this.nombre = nombre;
        this.distancia = distancia;
        this.cantidadFS = cantidadFS;
    }

    public Long getIdSustrateEdgeEntity() {
        return idSustrateEdgeEntity;
    }

    public void setIdSustrateEdgeEntity(Long idSustrateEdgeEntity) {
        this.idSustrateEdgeEntity = idSustrateEdgeEntity;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getDistancia() {
        return distancia;
    }

    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    public int getCantidadFS() {
        return cantidadFS;
    }

    public void setCantidadFS(int cantidadFS) {
        this.cantidadFS = cantidadFS;
    }

    public SustrateNodeEntity getNodoUno() {
        return nodoUno;
    }

    public void setNodoUno(SustrateNodeEntity nodoUno) {
        this.nodoUno = nodoUno;
    }

    public SustrateNodeEntity getNodoDos() {
        return nodoDos;
    }

    public void setNodoDos(SustrateNodeEntity nodoDos) {
        this.nodoDos = nodoDos;
    }

    public SustrateNetworkEntity getRedSustrato() {
        return redSustrato;
    }

    public void setRedSustrato(SustrateNetworkEntity redSustrato) {
        this.redSustrato = redSustrato;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSustrateEdgeEntity != null ? idSustrateEdgeEntity.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SustrateEdgeEntity)) {
            return false;
        }
        SustrateEdgeEntity other = (SustrateEdgeEntity) object;
        if ((this.idSustrateEdgeEntity == null && other.idSustrateEdgeEntity != null)
                || (this.idSustrateEdgeEntity != null
                && !this.idSustrateEdgeEntity.equals(other.idSustrateEdgeEntity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.una.pol.vone.simulator.entitys.SustrateEdge[ id=" + idSustrateEdgeEntity + " ]";
    }

}
