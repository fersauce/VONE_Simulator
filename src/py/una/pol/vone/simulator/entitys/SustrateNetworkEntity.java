package py.una.pol.vone.simulator.entitys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity que representa a la red sustrato
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/16/17
 */
@Entity
public class SustrateNetworkEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSustrateNetworkEntity;
    private String nombre;
    private int minCPU;
    private int maxCPU;
    private int cantidadFS;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @OneToMany(mappedBy = "redSustrato", cascade = CascadeType.ALL)
    private Collection<SustrateNodeEntity> nodosFisicos = new ArrayList<>();
    @OneToMany(mappedBy = "redSustrato", cascade = CascadeType.ALL)
    private Collection<SustrateEdgeEntity> enlacesFisicos = new ArrayList<>();

    /*Constructores*/
    public SustrateNetworkEntity() {
        super();
    }

    public SustrateNetworkEntity(String nombre, int identificador) {
        super();
        this.nombre = nombre;
    }

    /*Getters y Setters*/
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTotalCPU() {
        int totalCPU = 0;
        for (Iterator it = this.nodosFisicos.iterator(); it.hasNext();) {
            SustrateNodeEntity nodo = (SustrateNodeEntity) it.next();
            totalCPU += nodo.getCapacidadCPU();
        }
        return totalCPU;
    }

    public Integer getIdSustrateNetworkEntity() {
        return idSustrateNetworkEntity;
    }

    public void setIdSustrateNetworkEntity(Integer idSustrateNetworkEntity) {
        this.idSustrateNetworkEntity = idSustrateNetworkEntity;
    }

    public int getNroNodos() {
        return this.nodosFisicos.size();
    }

    public int getNroEnlaces() {
        return this.enlacesFisicos.size();
    }

    public Collection<SustrateNodeEntity> getNodosFisicos() {
        return nodosFisicos;
    }

    public void setNodosFisicos(Collection<SustrateNodeEntity> nodosFisicos) {
        this.nodosFisicos = nodosFisicos;
    }

    public int getMinCPU() {
        return minCPU;
    }

    public void setMinCPU(int minCPU) {
        this.minCPU = minCPU;
    }

    public int getMaxCPU() {
        return maxCPU;
    }

    public void setMaxCPU(int maxCPU) {
        this.maxCPU = maxCPU;
    }

    public int getCantidadFS() {
        return cantidadFS;
    }

    public void setCantidadFS(int cantidadFS) {
        this.cantidadFS = cantidadFS;
    }

    public Collection<SustrateEdgeEntity> getEnlacesFisicos() {
        return enlacesFisicos;
    }

    public void setEnlacesFisicos(Collection<SustrateEdgeEntity> 
            enlacesFisicos) {
        this.enlacesFisicos = enlacesFisicos;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSustrateNetworkEntity != null
                ? idSustrateNetworkEntity.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SustrateNetworkEntity)) {
            return false;
        }
        SustrateNetworkEntity other = (SustrateNetworkEntity) object;
        if ((this.idSustrateNetworkEntity == null && other.idSustrateNetworkEntity != null)
                || (this.idSustrateNetworkEntity != null
                && !this.idSustrateNetworkEntity.equals(other.idSustrateNetworkEntity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.una.pol.vone.simulator.entitys.SustrateNetwork[ "
                + "id=" + idSustrateNetworkEntity + " ]";
    }

    /**
     * Metodo que agrega un nodo fisico a la red.
     *
     * @param nodo objeto que almacena la informacion del nodo a relacionar con
     * la red.
     */
    public void agregarNodoFisico(SustrateNodeEntity nodo) {
        this.nodosFisicos.add(nodo);
    }

    /**
     * Metodo que agrega un enlace fisico a la red.
     *
     * @param enlace objeto que almacena la informacion del nodo a relacionar
     * con la red.
     */
    public void agregarEnlaceFisico(SustrateEdgeEntity enlace) {
        this.enlacesFisicos.add(enlace);
    }
}
