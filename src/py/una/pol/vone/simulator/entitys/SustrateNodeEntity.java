package py.una.pol.vone.simulator.entitys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Entity que representa al nodo sustrato
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/16/17
 */
@Entity
public class SustrateNodeEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idSustrateNodeEntity;
    private int identificador;
    private String nombre;
    private int capacidadCPU;
    @OneToMany(mappedBy = "nodoUno", cascade = CascadeType.ALL)
    private Collection<SustrateEdgeEntity> adyacentesNodoUno = new ArrayList<>();
    @OneToMany(mappedBy = "nodoDos", cascade = CascadeType.ALL)
    private Collection<SustrateEdgeEntity> adyacentesNodoDos = new ArrayList<>();
    @ManyToOne
    private SustrateNetworkEntity redSustrato;

    /*Constructores de clase*/
    public SustrateNodeEntity() {
        super();
    }

    public SustrateNodeEntity(int identificador, String nombre,
            int capacidadCPU) {
        super();
        this.identificador = identificador;
        this.nombre = nombre;
        this.capacidadCPU = capacidadCPU;
    }

    /*Getters y Setters*/
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCapacidadCPU() {
        return capacidadCPU;
    }

    public void setCapacidadCPU(int capacidadCPU) {
        this.capacidadCPU = capacidadCPU;
    }

    public Long getIdSustrateNodeEntity() {
        return idSustrateNodeEntity;
    }

    public void setIdSustrateNodeEntity(Long idSustrateNodeEntity) {
        this.idSustrateNodeEntity = idSustrateNodeEntity;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public SustrateNetworkEntity getRedSustrato() {
        return redSustrato;
    }

    public void setRedSustrato(SustrateNetworkEntity redSustrato) {
        this.redSustrato = redSustrato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSustrateNodeEntity != null ? idSustrateNodeEntity.hashCode() : 0);
        return hash;
    }

    public Collection getAdyacentesNodoUno() {
        return adyacentesNodoUno;
    }

    public void setAdyacentesNodoUno(Collection adyacentesNodoUno) {
        this.adyacentesNodoUno = adyacentesNodoUno;
    }

    public Collection getAdyacentesNodoDos() {
        return adyacentesNodoDos;
    }

    public void setAdyacentesNodoDos(Collection adyacentesNodoDos) {
        this.adyacentesNodoDos = adyacentesNodoDos;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SustrateNodeEntity)) {
            return false;
        }
        SustrateNodeEntity other = (SustrateNodeEntity) object;
        if ((this.idSustrateNodeEntity == null && other.idSustrateNodeEntity != null)
                || (this.idSustrateNodeEntity != null
                && !this.idSustrateNodeEntity.equals(other.idSustrateNodeEntity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.una.pol.vone.simulator.entitys.SustrateNode[ "
                + "id=" + idSustrateNodeEntity + " ]";
    }

    /**
     * Metodo para agregar un enlace a la lista de adyacentes como nodo uno.
     *
     * @param enlace objeto que almacena los datos relacionados al enlace a
     * relacionarlo.
     */
    public void agregarAdyacentesNodoUno(SustrateEdgeEntity enlace) {
        this.adyacentesNodoUno.add(enlace);
    }

    /**
     * Metodo para agregar un enlace a lista de adyacentes como nodo dos
     *
     * @param enlace.objeto que almacena los datos relacionados al enlace a 
     * relacionarlo.
     */
    public void agregarAdyacentesNodoDos(SustrateEdgeEntity enlace) {
        this.adyacentesNodoDos.add(enlace);
    }
}
