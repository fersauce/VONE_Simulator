package py.una.pol.vone.simulator.entitys;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Clase que representa a la entidad que almacenara los enlaces virtuales.
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/16/17
 */
@Entity
public class VirtualEdgeEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idVirtualEdgeEntity;
    private int identificador;
    private String nombre;
    private int cantidadFS;
    private boolean mapeado;
    @ManyToOne
    private VirtualNodeEntity nodoUno;
    @ManyToOne
    private VirtualNodeEntity nodoDos;
    @ManyToOne
    private VirtualNetworkEntity redVirtual;
    
    /*Constructores*/
    public VirtualEdgeEntity() {
        super();
        this.mapeado = false;
    }

    public VirtualEdgeEntity(String nombre, int cantidadFS) {
        super();
        this.nombre = nombre;
        this.cantidadFS = cantidadFS;
        this.mapeado = false;
    }

    /*Setters y Getters*/
    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCantidadFS() {
        return cantidadFS;
    }

    public void setCantidadFS(int cantidadFS) {
        this.cantidadFS = cantidadFS;
    }

    public boolean isMapeado() {
        return mapeado;
    }

    public void setMapeado(boolean mapeado) {
        this.mapeado = mapeado;
    }

    public Long getIdVirtualEdgeEntity() {
        return idVirtualEdgeEntity;
    }

    public void setIdVirtualEdgeEntity(Long idVirtualEdgeEntity) {
        this.idVirtualEdgeEntity = idVirtualEdgeEntity;
    }

    public VirtualNodeEntity getNodoUno() {
        return nodoUno;
    }

    public void setNodoUno(VirtualNodeEntity nodoUno) {
        this.nodoUno = nodoUno;
    }

    public VirtualNodeEntity getNodoDos() {
        return nodoDos;
    }

    public void setNodoDos(VirtualNodeEntity nodoDos) {
        this.nodoDos = nodoDos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVirtualEdgeEntity != null ? idVirtualEdgeEntity.hashCode() : 0);
        return hash;
    }

    public VirtualNetworkEntity getRedVirtual() {
        return redVirtual;
    }

    public void setRedVirtual(VirtualNetworkEntity redVirtual) {
        this.redVirtual = redVirtual;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof VirtualEdgeEntity)) {
            return false;
        }
        VirtualEdgeEntity other = (VirtualEdgeEntity) object;
        if ((this.idVirtualEdgeEntity == null && other.idVirtualEdgeEntity != null)
                || (this.idVirtualEdgeEntity != null
                && !this.idVirtualEdgeEntity.equals(other.idVirtualEdgeEntity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.una.pol.vone.simulator.entitys.VirtualEdge[ "
                + " id=" + idVirtualEdgeEntity + " ]";
    }

}
