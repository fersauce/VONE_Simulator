package py.una.pol.vone.simulator.entitys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Entity que representa a la red virtual
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/16/17
 */
@Entity
public class VirtualNetworkEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idVirtualNetworkEntity;
    private int identificador;
    private String nombre;
    private int tiempoInicial;
    private int tiempoFinal;
    private boolean mapeado;
    @OneToMany(targetEntity = VirtualNodeEntity.class, mappedBy = "redVirtual",
            cascade = CascadeType.ALL)
    private Collection<VirtualNodeEntity> nodosVirtuales = new ArrayList<>();
    @OneToMany(targetEntity = VirtualEdgeEntity.class, mappedBy = "redVirtual",
            cascade = CascadeType.ALL)
    private Collection<VirtualEdgeEntity> enlacesVirtuales = new ArrayList<>();
    @ManyToOne
    private VirtualNetworkListEntity lista;

    /*Constructores*/
    public VirtualNetworkEntity() {
        super();
        this.mapeado = false;
    }

    public VirtualNetworkEntity(String nombre) {
        super();
        this.nombre = nombre;
        this.mapeado = false;
    }

    /*Setters y Getters*/
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNroNodos() {
        return this.nodosVirtuales.size();
    }

    public int getNroEnlaces() {
        return this.enlacesVirtuales.size();
    }

    public int getTotalCPU() {
        int totalCPU = 0;
        for (Iterator it = this.nodosVirtuales.iterator(); it.hasNext();) {
            VirtualNodeEntity nodo = (VirtualNodeEntity) it.next();
            totalCPU += nodo.getCapacidadCPU();
        }
        return totalCPU;
    }

    public boolean isMapeado() {
        return mapeado;
    }

    public void setMapeado(boolean mapeado) {
        this.mapeado = mapeado;
    }

    public Long getIdVirtualNetworkEntity() {
        return idVirtualNetworkEntity;
    }

    public void setIdVirtualNetworkEntity(Long idVirtualNetworkEntity) {
        this.idVirtualNetworkEntity = idVirtualNetworkEntity;
    }

    public int getTiempoInicial() {
        return tiempoInicial;
    }

    public void setTiempoInicial(int tiempoInicial) {
        this.tiempoInicial = tiempoInicial;
    }

    public int getTiempoFinal() {
        return tiempoFinal;
    }

    public void setTiempoFinal(int tiempoFinal) {
        this.tiempoFinal = tiempoFinal;
    }

    public Collection getNodosVirtuales() {
        return nodosVirtuales;
    }

    public void setNodosVirtuales(Collection nodosVirtuales) {
        this.nodosVirtuales = nodosVirtuales;
    }

    public VirtualNetworkListEntity getLista() {
        return lista;
    }

    public void setLista(VirtualNetworkListEntity lista) {
        this.lista = lista;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVirtualNetworkEntity != null ? idVirtualNetworkEntity.hashCode() : 0);
        return hash;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public Collection getEnlacesVirtuales() {
        return enlacesVirtuales;
    }

    public void setEnlacesVirtuales(Collection enlacesVirtuales) {
        this.enlacesVirtuales = enlacesVirtuales;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof VirtualNetworkEntity)) {
            return false;
        }
        VirtualNetworkEntity other = (VirtualNetworkEntity) object;
        if ((this.idVirtualNetworkEntity == null && other.idVirtualNetworkEntity != null)
                || (this.idVirtualNetworkEntity != null
                && !this.idVirtualNetworkEntity.equals(other.idVirtualNetworkEntity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.una.pol.vone.simulator.entitys.VirtualNetwork[ "
                + "id=" + idVirtualNetworkEntity + " ]";
    }

    /**
     * Metodo para agregar un nodo a la red
     *
     * @param nodo objeto con informacion del nodo a ser agregado a la red.
     */
    public void agregarNodoVirtual(VirtualNodeEntity nodo) {
        this.nodosVirtuales.add(nodo);
    }

    /**
     * Metodo para agregar un enlace a la red.
     *
     * @param enlace objeto con informacion del enlace a ser agregado a la red.
     */
    public void agregarEnlaceVirtual(VirtualEdgeEntity enlace) {
        this.enlacesVirtuales.add(enlace);
    }
}
