package py.una.pol.vone.simulator.entitys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity que almacenara la lista de redes virtuales con todos los parametros
 * que fueron utilizados para su generacion.
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/16/17
 */
@Entity
public class VirtualNetworkListEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nombre;
    private boolean esEstatico;
    private int ciclos;
    private int erlangs;
    private int holdingTime;
    private double lambda;
    private int minCPU;
    private int maxCPU;
    private int minFS;
    private int maxFS;
    private int minNumeroNodos;
    private int maxNumeroNodos;
    private int cantidadRedes;
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @OneToMany(targetEntity = VirtualNetworkEntity.class, mappedBy = "lista",
            cascade = CascadeType.ALL)
    private Collection<VirtualNetworkEntity> conjuntoRedes = new ArrayList<>();

    /*Constructores*/
    public VirtualNetworkListEntity() {
        super();
    }

    public VirtualNetworkListEntity(String nombre, int erlangs, int holdingTime,
            int minCPU, int maxCPU, int minFS, int maxFS) {
        this.nombre = nombre;
        this.erlangs = erlangs;
        this.holdingTime = holdingTime;
        this.minCPU = minCPU;
        this.maxCPU = maxCPU;
        this.minFS = minFS;
        this.maxFS = maxFS;
    }

    /*Setters y Getters*/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getErlangs() {
        return erlangs;
    }

    public void setErlangs(int erlangs) {
        this.erlangs = erlangs;
    }

    public int getHoldingTime() {
        return holdingTime;
    }

    public void setHoldingTime(int holdingTime) {
        this.holdingTime = holdingTime;
    }

    public int getMinCPU() {
        return minCPU;
    }

    public void setMinCPU(int minCPU) {
        this.minCPU = minCPU;
    }

    public int getMaxCPU() {
        return maxCPU;
    }

    public void setMaxCPU(int maxCPU) {
        this.maxCPU = maxCPU;
    }

    public int getMinFS() {
        return minFS;
    }

    public void setMinFS(int minFS) {
        this.minFS = minFS;
    }

    public int getMaxFS() {
        return maxFS;
    }

    public void setMaxFS(int maxFS) {
        this.maxFS = maxFS;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Collection<VirtualNetworkEntity> getConjuntoRedes() {
        return conjuntoRedes;
    }

    public void setConjuntoRedes(Collection<VirtualNetworkEntity> conjuntoRedes) {
        this.conjuntoRedes = conjuntoRedes;
    }

    public double getLambda() {
        return lambda;
    }

    public void setLambda(double lambda) {
        this.lambda = lambda;
    }

    public boolean isEsEstatico() {
        return esEstatico;
    }

    public void setEsEstatico(boolean esEstatico) {
        this.esEstatico = esEstatico;
    }

    public int getCiclos() {
        return ciclos;
    }

    public void setCiclos(int ciclos) {
        this.ciclos = ciclos;
    }

    public int getMinNumeroNodos() {
        return minNumeroNodos;
    }

    public void setMinNumeroNodos(int minNumeroNodos) {
        this.minNumeroNodos = minNumeroNodos;
    }

    public int getMaxNumeroNodos() {
        return maxNumeroNodos;
    }

    public void setMaxNumeroNodos(int maxNumeroNodos) {
        this.maxNumeroNodos = maxNumeroNodos;
    }

    public int getCantidadRedes() {
        return cantidadRedes;
    }

    public void setCantidadRedes(int cantidadRedes) {
        this.cantidadRedes = cantidadRedes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object object) {
        /*Warning - this method 
        won't work in the case the id fields are not set*/
        if (!(object instanceof VirtualNetworkListEntity)) {
            return false;
        }
        VirtualNetworkListEntity other = (VirtualNetworkListEntity) object;
        if ((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.una.pol.vone.simulator.entitys.VirtualNetworkList[ id="
                + id + " ]";
    }

    /**
     * Metodo para agregar una red virtual a la lista.
     *
     * @param redVirtual objeto con informacion de la red virtual a ser agregada
     * a la lista de generados.
     */
    public void agregarRedVirtual(VirtualNetworkEntity redVirtual) {
        this.conjuntoRedes.add(redVirtual);
    }
}
