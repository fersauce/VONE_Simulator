package py.una.pol.vone.simulator.entitys;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Clase que representa a la entidad que almacenara los nodos virtuales
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/11/17
 */
@Entity
public class VirtualNodeEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idVirtualNodeEntity;
    private String nombre;
    private int identificador;
    private int capacidadCPU;
    private boolean mapeado;
    @OneToMany(mappedBy = "nodoUno", cascade = CascadeType.ALL)
    private Collection<VirtualEdgeEntity> adyacentesNodoUno = new ArrayList<>();
    @OneToMany(mappedBy = "nodoDos", cascade = CascadeType.ALL)
    private Collection<VirtualEdgeEntity> adyacentesNodoDos = new ArrayList<>();
    @ManyToOne
    private VirtualNetworkEntity redVirtual;

    /*Constructores*/
    public VirtualNodeEntity() {
        super();
        this.mapeado = false;
    }

    public VirtualNodeEntity(String nombre, int identificador,
            int capacidadCPU) {
        super();
        this.nombre = nombre;
        this.identificador = identificador;
        this.capacidadCPU = capacidadCPU;
        this.mapeado = false;
    }

    /*Setters y Getters*/
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public int getCapacidadCPU() {
        return capacidadCPU;
    }

    public void setCapacidadCPU(int capacidadCPU) {
        this.capacidadCPU = capacidadCPU;
    }

    public boolean isMapeado() {
        return mapeado;
    }

    public void setMapeado(boolean mapeado) {
        this.mapeado = mapeado;
    }

    public Long getIdVirtualNodeEntity() {
        return idVirtualNodeEntity;
    }

    public void setIdVirtualNodeEntity(Long idVirtualNodeEntity) {
        this.idVirtualNodeEntity = idVirtualNodeEntity;
    }

    public Collection getAdyacentesNodoUno() {
        return adyacentesNodoUno;
    }

    public void setAdyacentesNodoUno(Collection adyacentesNodoUno) {
        this.adyacentesNodoUno = adyacentesNodoUno;
    }

    public Collection getAdyacentesNodoDos() {
        return adyacentesNodoDos;
    }

    public void setAdyacentesNodoDos(Collection adyacentesNodoDos) {
        this.adyacentesNodoDos = adyacentesNodoDos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idVirtualNodeEntity != null ? idVirtualNodeEntity.hashCode() : 0);
        return hash;
    }

    public VirtualNetworkEntity getRedVirtual() {
        return redVirtual;
    }

    public void setRedVirtual(VirtualNetworkEntity redVirtual) {
        this.redVirtual = redVirtual;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof VirtualNodeEntity)) {
            return false;
        }
        VirtualNodeEntity other = (VirtualNodeEntity) object;
        if ((this.idVirtualNodeEntity == null && other.idVirtualNodeEntity != null)
                || (this.idVirtualNodeEntity != null
                && !this.idVirtualNodeEntity.equals(other.idVirtualNodeEntity))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.una.pol.vone.simulator.entitys.VirtualNode[ "
                + "id=" + idVirtualNodeEntity + " ]";
    }

    /**
     * Metodo para agregar un enlace relacionado a este nodo en el cual el mismo
     * esta como nodo uno del enlace.
     *
     * @param enlace objeto con informacion del enlace a relacionar.
     */
    public void agregarEnlaceAdyacenteNodoUno(VirtualEdgeEntity enlace) {
        this.getAdyacentesNodoUno().add(enlace);
    }

    /**
     * Metodo para agregar un enlace relacionado a este nodo en el cual el mismo
     * esta como nodo dos del enlace.
     *
     * @param enlace objeto con informacion del enlace a relacionar.
     */
    public void agregarEnlaceAdyacenteNodoDos(VirtualEdgeEntity enlace) {
        this.getAdyacentesNodoDos().add(enlace);
    }
}
