/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.pol.vone.simulator.gui;

import java.awt.Toolkit;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import py.una.pol.vone.simulator.DAO.SustrateEdgeDAO;
import py.una.pol.vone.simulator.DAO.SustrateNetworkDAO;
import py.una.pol.vone.simulator.DAO.SustrateNodeDAO;
import py.una.pol.vone.simulator.DAO.VirtualEdgeDAO;
import py.una.pol.vone.simulator.DAO.VirtualNetworkDAO;
import py.una.pol.vone.simulator.DAO.VirtualNodeDAO;
import py.una.pol.vone.simulator.model.SustrateNetwork;
import py.una.pol.vone.simulator.model.VirtualNetworkList;
import py.una.pol.vone.simulator.util.DataManagement;
import py.una.pol.vone.simulator.util.NetworkGenerator;
import static py.una.pol.vone.simulator.util.Simulations.ejecutarPruebaEstatica;

/**
 *
 * @author Fernando Saucedo
 */
public class Main {
    public static EntityManager entityManager;
    
    public static void main(String[] args) {
        
        SimuladorGUI ventana = new SimuladorGUI();
        
        EntityManagerFactory emfactory = Persistence
                .createEntityManagerFactory("VONE_SimulatorPU");
         entityManager = emfactory.createEntityManager();
         
        SustrateNetworkDAO sustrateNetworkDAO = new SustrateNetworkDAO();
        SustrateNodeDAO sustrateNodeDAO = new SustrateNodeDAO();
        SustrateEdgeDAO sustrateEdgeDAO = new SustrateEdgeDAO();
        VirtualNetworkDAO virtualNetworkDAO = new VirtualNetworkDAO();
        VirtualNodeDAO virtualNodeDAO = new VirtualNodeDAO();
        VirtualEdgeDAO virtualEdgeDAO = new VirtualEdgeDAO();
        ventana.setVisible(true);
        
        /**
         * Carga automatizada de VNR
         * Se debe comentar las lineas 34 y 46 de esta clase 
         */
        /*
        int cantVNR = 100;
        VirtualNetworkList vn = null;
        
        for(int i = 10; i<= cantVNR; i += 10) {
            vn = NetworkGenerator.generarRequerimientosEstaticos(i, 1, 5, 2, 8, 2, 5, entityManager);

            DataManagement.persistirListaRedesVirtuales(vn, entityManager);
            vn = null;
            System.out.println("Conjunto de VNR creado exitosamente: " + String.valueOf(i));
        }
        */
        
        /**
         * Ejecución automatizada sin GUI.
         * Descomentar a partir de las siguientes lineas para realizar varias
         * ejecuciones de una sola vez con varias VNR
         * Se debe comentar las lineas 34 y 46 de esta clase
         */
        
        /*
        String archivoResultado = null;
        SustrateNetwork sn = null;
        VirtualNetworkList vn = null;

        ArrayList<String> listaMetricas = new ArrayList<>(); 
        listaMetricas.add("Tasa de Rechazo");
        listaMetricas.add("Utilización promedio de enlace");
        listaMetricas.add("Costo del Embedding");
        listaMetricas.add("Revenue");
        listaMetricas.add("Relación Revenue-Costo");
        listaMetricas.add("VONE N.P.");

        for(int i= 31; i<=40; i++){
            sn = DataManagement.obtenerRedFisica("RedFisica21USNet");
            vn = DataManagement.obtenerConjuntoRedesVirtuales("Conjunto"+String.valueOf(i), entityManager, true);         
            archivoResultado = ejecutarPruebaEstatica(sn, vn, "Voraz Estático", listaMetricas);

            // Imprimimos un mensaje de finalizacion
            if (archivoResultado != null) {
                //System.out.println("Resultado exitoso! Conjunto" + String.valueOf(i));
            } else {
                System.out.println("Resultado fallido");
            }
            sn = null;
            vn = null;
        }
        Toolkit.getDefaultToolkit().beep();
        */
    }
}
