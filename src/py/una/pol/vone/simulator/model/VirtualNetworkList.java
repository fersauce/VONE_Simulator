package py.una.pol.vone.simulator.model;

import java.util.ArrayList;
import java.util.Date;

/**
 * Modelo que almacenara los parametros y el conjunto de redes virtuales
 * generados con esos valores.
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 */
public class VirtualNetworkList {

    private Long id;
    private String nombre;
    private boolean esEstatico;
    private int ciclos;
    private int erlangs;
    private int holdingTime;
    private double lambda;
    private int minCPU;
    private int maxCPU;
    private int minFS;
    private int maxFS;
    private int minNumeroNodos;
    private int maxNumeroNodos;
    private int cantidadRedes;
    private Date fecha;
    private ArrayList<VirtualNetwork> conjuntoRedes;

    /*Constructores*/
    public VirtualNetworkList() {
        super();
    }

    public VirtualNetworkList(String nombre, int erlangs, int holdingTime,
            int minCPU, int maxCPU, int minFS, int maxFS) {
        this.nombre = nombre;
        this.erlangs = erlangs;
        this.holdingTime = holdingTime;
        this.minCPU = minCPU;
        this.maxCPU = maxCPU;
        this.minFS = minFS;
        this.maxFS = maxFS;
    }

    /*Setters y Getters*/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getErlangs() {
        return erlangs;
    }

    public void setErlangs(int erlangs) {
        this.erlangs = erlangs;
    }

    public int getHoldingTime() {
        return holdingTime;
    }

    public void setHoldingTime(int holdingTime) {
        this.holdingTime = holdingTime;
    }

    public double getLambda() {
        return lambda;
    }

    public void setLambda(double lambda) {
        this.lambda = lambda;
    }

    public int getMinCPU() {
        return minCPU;
    }

    public void setMinCPU(int minCPU) {
        this.minCPU = minCPU;
    }

    public int getMaxCPU() {
        return maxCPU;
    }

    public void setMaxCPU(int maxCPU) {
        this.maxCPU = maxCPU;
    }

    public int getMinFS() {
        return minFS;
    }

    public void setMinFS(int minFS) {
        this.minFS = minFS;
    }

    public int getMaxFS() {
        return maxFS;
    }

    public void setMaxFS(int maxFS) {
        this.maxFS = maxFS;
    }

    public ArrayList<VirtualNetwork> getConjuntoRedes() {
        return conjuntoRedes;
    }

    public void setConjuntoRedes(ArrayList<VirtualNetwork> conjuntoRedes) {
        this.conjuntoRedes = conjuntoRedes;
    }

    public boolean isEsEstatico() {
        return esEstatico;
    }

    public void setEsEstatico(boolean esEstatico) {
        this.esEstatico = esEstatico;
    }

    public int getCiclos() {
        return ciclos;
    }

    public void setCiclos(int ciclos) {
        this.ciclos = ciclos;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getMinNumeroNodos() {
        return minNumeroNodos;
    }

    public void setMinNumeroNodos(int minNumeroNodos) {
        this.minNumeroNodos = minNumeroNodos;
    }

    public int getMaxNumeroNodos() {
        return maxNumeroNodos;
    }

    public void setMaxNumeroNodos(int maxNumeroNodos) {
        this.maxNumeroNodos = maxNumeroNodos;
    }

    public int getCantidadRedes() {
        return cantidadRedes;
    }

    public void setCantidadRedes(int cantidadRedes) {
        this.cantidadRedes = cantidadRedes;
    }

    /**
     * Re-implementacion del metodo toString.
     *
     * @return cadena del conjunto de redes virtuales.
     */
    @Override
    public String toString() {
        String cadena = new String();
        cadena = cadena.concat("Nombre del Conjunto: " + this.nombre + ".\n");
        cadena = cadena.concat("Tipo de conjunto generado: ");
        if (this.esEstatico) {
            cadena = cadena.concat("Est\u00e1tico.\n");
            cadena = cadena.concat("Cantidad de VNRs solicitadas por el "
                    + "usuario: " + this.cantidadRedes + ".\n");

        } else {
            cadena = cadena.concat("Din\u00e1mico.\n");
            cadena = cadena.concat("Cantidad de Ciclos: " + this.ciclos 
                    + ".\n");
            cadena = cadena.concat("Tr\u00e1fico de red (erlangs): "
                    + this.erlangs + ".\n");
            cadena = cadena.concat("Tiempo de vida promedio: "
                    + this.holdingTime + " ciclos.\n");
            cadena = cadena.concat("Valor de lambda calculado: "
                    + this.lambda + ".\n");
        }
        cadena = cadena.concat("M\u00ednimo de CPU cargado: " + this.minCPU
                + ".\n");
        cadena = cadena.concat("M\u00e1ximo de CPU cargado: " + this.maxCPU
                + ".\n");
        cadena = cadena.concat("M\u00ednimo de FS cargado: " + this.minFS
                + ".\n");
        cadena = cadena.concat("M\u00e1ximo de FS cargado: " + this.maxFS
                + ".\n");
        cadena = cadena.concat("M\u00ednimo n\u00famero de nodos cargado: "
                + this.minNumeroNodos + ".\n");
        cadena = cadena.concat("M\u00e1ximo n\u00famero de nodos cargado: "
                + this.maxNumeroNodos + ".\n");
        int i = 1;
        for (VirtualNetwork redVirtual : this.conjuntoRedes) {
            cadena = cadena.concat("--------\n");
            cadena = cadena.concat("Red N\u00famero " + i++ + ":\n");
            if (!this.esEstatico) {
                cadena = cadena.concat("Ciclo de entrada: " + redVirtual.
                        getTiempoInicial() + "\n");
                cadena = cadena.concat("Ciclo de salida: " + redVirtual.
                        getTiempoFinal() + "\n");
            }
            cadena = cadena.concat(redVirtual.toString() + "\n");
            cadena = cadena.concat("--------\n");
        }
        return cadena;
    }
}
