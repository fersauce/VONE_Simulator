package py.una.pol.vone.simulator.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.EntityManager;
import py.una.pol.vone.simulator.DAO.SustrateEdgeDAO;
import py.una.pol.vone.simulator.DAO.SustrateNetworkDAO;
import py.una.pol.vone.simulator.DAO.SustrateNodeDAO;
import py.una.pol.vone.simulator.DAO.VirtualEdgeDAO;
import py.una.pol.vone.simulator.DAO.VirtualNetworkDAO;
import py.una.pol.vone.simulator.DAO.VirtualNetworkListDAO;
import py.una.pol.vone.simulator.DAO.VirtualNodeDAO;
import py.una.pol.vone.simulator.entitys.SustrateEdgeEntity;
import py.una.pol.vone.simulator.entitys.SustrateNetworkEntity;
import py.una.pol.vone.simulator.entitys.SustrateNodeEntity;
import py.una.pol.vone.simulator.entitys.VirtualEdgeEntity;
import py.una.pol.vone.simulator.entitys.VirtualNetworkEntity;
import py.una.pol.vone.simulator.entitys.VirtualNetworkListEntity;
import py.una.pol.vone.simulator.entitys.VirtualNodeEntity;
import py.una.pol.vone.simulator.gui.Main;
import py.una.pol.vone.simulator.model.SustrateEdge;
import py.una.pol.vone.simulator.model.SustrateNetwork;
import py.una.pol.vone.simulator.model.SustrateNode;
import py.una.pol.vone.simulator.model.VirtualEdge;
import py.una.pol.vone.simulator.model.VirtualNetwork;
import py.una.pol.vone.simulator.model.VirtualNetworkList;
import py.una.pol.vone.simulator.model.VirtualNode;

/**
 * Clase en donde se implementara todo lo relacionado al relacionamiento entre
 * el modelo y la base de datos en la que se almacenaran los datos generados.
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0, 03/11/17
 */
public class DataManagement {

    /**
     * Metodo que persistira la red fisica que se ha creado.
     *
     * @param redSustrato objeto que almacena todos los detalles de la red
     * fisica a ser persistida.
     * @param entityManager instancia para poder conectarse a la BD.
     */
    public static void persistirRedFisica(SustrateNetwork redSustrato,
            EntityManager entityManager) {
        /*Se inicializan los DAOS*/
        SustrateNetworkDAO sustrateNetworkDAO = new SustrateNetworkDAO();
        SustrateNodeDAO sustrateNodeDAO = new SustrateNodeDAO();
        SustrateEdgeDAO sustrateEdgeDAO = new SustrateEdgeDAO();
        /*Se inicia la transaccion*/
        entityManager.getTransaction().begin();
        try {
            /*Se genera primero la red fisica y se persiste*/
            SustrateNetworkEntity redFisica = new SustrateNetworkEntity();
            redFisica.setNombre(redSustrato.getNombre());
            redFisica.setMinCPU(redSustrato.getMinCPU());
            redFisica.setMaxCPU(redSustrato.getMaxCPU());
            redFisica.setCantidadFS(redSustrato.getCantidadFS());
            if (redFisica.getFecha() == null) {
                redFisica.setFecha(new Date());
            }
            sustrateNetworkDAO.agregar(redFisica, entityManager);
            /*Luego se crean los nodos fisicos, para cada uno se crea y se 
             *enlaza a la red fisica que corresponde y luego se persiste; y luego 
             *a la red fisica se enlaza el nodo creado y se persiste el cambio.
             */
            redSustrato.getNodosFisicos().forEach((nodoSustrato) -> {
                SustrateNodeEntity nodoFisico = new SustrateNodeEntity();
                nodoFisico.setNombre(nodoSustrato.getNombre());
                nodoFisico.setIdentificador(nodoSustrato.getIdentificador());
                nodoFisico.setCapacidadCPU(nodoSustrato.getCapacidadCPU());
                nodoFisico.setRedSustrato(redFisica);
                sustrateNodeDAO.agregar(nodoFisico, entityManager);
                redFisica.agregarNodoFisico(nodoFisico);
                sustrateNetworkDAO.actualizar(redFisica, entityManager);
            });
            /*Por ultimo, se crean los enlaces fisicos, cada uno se crea, se 
            busca los nodos fisicos que se relacionaran a la misma y luego se 
            persiste; luego para cada nodo se agrega el enlace a la relacion 
            (ya sea si se enlaza como nodo Uno o nodo Dos) y luego se 
            persisten, y por ultimo se agrega a la red fisica la relacion con 
            la misma y se persiste dicha actualizacion.*/
            redSustrato.getEnlacesFisicos().forEach((enlaceSustrato) -> {
                SustrateEdgeEntity enlaceFisico = new SustrateEdgeEntity();
                enlaceFisico.setNombre(enlaceSustrato.getNombre());
                enlaceFisico.setIdentificador(enlaceSustrato.
                        getIdentificador());
                enlaceFisico.setDistancia(enlaceSustrato.getDistancia());
                enlaceFisico.setCantidadFS(enlaceSustrato.getCantidadFS());
                enlaceFisico.setRedSustrato(redFisica);
                sustrateEdgeDAO.agregar(enlaceFisico, entityManager);
                SustrateNodeEntity nodoUno = sustrateNodeDAO.
                        obtenerNodoPorIdentificador(enlaceSustrato.
                                getNodoUno().
                                getIdentificador(), redFisica, entityManager);
                SustrateNodeEntity nodoDos = sustrateNodeDAO.
                        obtenerNodoPorIdentificador(enlaceSustrato.
                                getNodoDos().
                                getIdentificador(), redFisica, entityManager);
                enlaceFisico.setNodoUno(nodoUno);
                enlaceFisico.setNodoDos(nodoDos);
                enlaceFisico.setRedSustrato(redFisica);
                sustrateEdgeDAO.agregar(enlaceFisico, entityManager);
                nodoUno.agregarAdyacentesNodoUno(enlaceFisico);
                sustrateNodeDAO.actualizar(nodoUno, entityManager);
                nodoDos.agregarAdyacentesNodoDos(enlaceFisico);
                sustrateNodeDAO.actualizar(nodoDos, entityManager);
                redFisica.agregarEnlaceFisico(enlaceFisico);
                sustrateNetworkDAO.actualizar(redFisica, entityManager);
            });
            /*Por ultimo, si no hubo errores, se confirma la transaccion*/
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("No se pudo almacenar la red fisica.");
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    /**
     * Metodo que persistira el conjunto de redes virtuales que se ha creado
     * para una prueba, tanto sea un conjunto estatico o dinamico.
     *
     * @param redesVirtuales objeto que almacena todas las redes virtuales que
     * han sido generadas y quiere ser persistida.
     * @param entityManager instancia para poder conectarse a la BD.
     */
    public static void persistirListaRedesVirtuales(VirtualNetworkList redesVirtuales,
            EntityManager entityManager) {
        /*Se inicializan los DAOS*/
        VirtualNetworkListDAO networkListDAO = new VirtualNetworkListDAO();
        VirtualNetworkDAO networkDAO = new VirtualNetworkDAO();
        VirtualNodeDAO nodeDAO = new VirtualNodeDAO();
        VirtualEdgeDAO edgeDAO = new VirtualEdgeDAO();
        /*Se inicia la transaccion*/
        entityManager.getTransaction().begin();
        try {
            /*Se crea el conjunto de redes virtuales y se persiste*/
            VirtualNetworkListEntity conjunto
                    = new VirtualNetworkListEntity();
            conjunto.setNombre(redesVirtuales.getNombre());
            conjunto.setEsEstatico(redesVirtuales.
                    isEsEstatico());
            conjunto.setCiclos(redesVirtuales.getCiclos());
            conjunto.setErlangs(redesVirtuales.getErlangs());
            conjunto.setHoldingTime(redesVirtuales.
                    getHoldingTime());
            conjunto.setLambda(redesVirtuales.getLambda());
            conjunto.setMinCPU(redesVirtuales.getMinCPU());
            conjunto.setMaxCPU(redesVirtuales.getMaxCPU());
            conjunto.setMinFS(redesVirtuales.getMinFS());
            conjunto.setMaxFS(redesVirtuales.getMaxFS());
            conjunto.setMinNumeroNodos(redesVirtuales.getMinNumeroNodos());
            conjunto.setMaxNumeroNodos(redesVirtuales.getMaxNumeroNodos());
            conjunto.setCantidadRedes(redesVirtuales.getCantidadRedes());
            if (conjunto.getFecha() == null) {
                conjunto.setFecha(new Date());
            }
            networkListDAO.agregar(conjunto, entityManager);
            /*Ahora se cargaran los requerimientos de redes virtuales y luego 
             *se persistiran y se enlazaran al conjunto 
             *generado
             */
            redesVirtuales.getConjuntoRedes().forEach((redVirtual) -> {
                /*Primero se genera la red Virtual y se persiste*/
                VirtualNetworkEntity redNueva = new VirtualNetworkEntity();
                redNueva.setIdentificador(redVirtual.getIdentificador());
                redNueva.setNombre(redVirtual.getNombre());
                redNueva.setTiempoInicial(redVirtual.getTiempoInicial());
                redNueva.setTiempoFinal(redVirtual.getTiempoFinal());
                redNueva.setMapeado(redVirtual.isMapeado());
                networkDAO.agregar(redNueva, entityManager);
                /*Ahora se procede a cargar los nodos virtuales y realizar las 
                 *relaciones entre la red creada y los nodos creados aqui, y 
                 *persistir el cambio.
                 */
                redVirtual.getNodosVirtuales().forEach((nodoVirtual) -> {
                    VirtualNodeEntity nodoNuevo = new VirtualNodeEntity();
                    nodoNuevo.setIdentificador(nodoVirtual.getIdentificador());
                    nodoNuevo.setNombre(nodoVirtual.getNombre());
                    nodoNuevo.setCapacidadCPU(nodoVirtual.getCapacidadCPU());
                    nodoNuevo.setMapeado(nodoVirtual.isMapeado());
                    nodoNuevo.setRedVirtual(redNueva);
                    nodeDAO.agregar(nodoNuevo, entityManager);
                    redNueva.agregarNodoVirtual(nodoNuevo);
                    networkDAO.actualizar(redNueva, entityManager);
                });
                /*Luego se procede a cargar los enlaces, se crea los enlaces, 
                 *se buscan los nodos uno y dos de cada enlace, se hacen las 
                 *relaciones y todo luego se relaciona con la red nueva 
                 *generada y ser persiste el cambio.
                 */
                redVirtual.getEnlacesVirtuales().forEach((enlaceVirtual) -> {
                    VirtualEdgeEntity enlaceNuevo = new VirtualEdgeEntity();
                    enlaceNuevo.setIdentificador(enlaceVirtual.getIdentificador());
                    enlaceNuevo.setNombre(enlaceVirtual.getNombre());
                    enlaceNuevo.setCantidadFS(enlaceVirtual.getCantidadFS());
                    enlaceNuevo.setMapeado(enlaceVirtual.isMapeado());
                    VirtualNodeEntity nodoUno = nodeDAO.
                            obtenerNodoPorIdentificador(enlaceVirtual.
                                    getNodoUno().getIdentificador(), redNueva,
                                    entityManager);
                    VirtualNodeEntity nodoDos = nodeDAO.
                            obtenerNodoPorIdentificador(enlaceVirtual.
                                    getNodoDos().getIdentificador(), redNueva,
                                    entityManager);
                    enlaceNuevo.setNodoUno(nodoUno);
                    enlaceNuevo.setNodoDos(nodoDos);
                    enlaceNuevo.setRedVirtual(redNueva);
                    edgeDAO.agregar(enlaceNuevo, entityManager);
                    nodoUno.agregarEnlaceAdyacenteNodoUno(enlaceNuevo);
                    nodeDAO.actualizar(nodoUno, entityManager);
                    nodoDos.agregarEnlaceAdyacenteNodoDos(enlaceNuevo);
                    nodeDAO.actualizar(nodoDos, entityManager);
                    redNueva.agregarEnlaceVirtual(enlaceNuevo);
                    networkDAO.actualizar(redNueva, entityManager);
                });
                redNueva.setLista(conjunto);
                networkDAO.actualizar(redNueva, entityManager);
                conjunto.agregarRedVirtual(redNueva);
                networkListDAO.actualizar(conjunto, entityManager);
            });
            /*Por ultimo, si no hubo errores en la carga, se comitea la 
             *transaccion.
             */
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("No se persistio el conjunto de redes virtuales.");
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    /**
     * Metodo que se encarga de traer todas las redes fisicas que se encuentran
     * almacenadas en la BD.
     *
     * @param entityManager instancia para poder conectarse a la BD.
     * @return todas las redes fisicas que estan guardadas en la BD
     */
    public static ArrayList<SustrateNetwork>
            obtenerTotalRedesFisicas(EntityManager entityManager) {
        SustrateNetworkDAO networkDAO = new SustrateNetworkDAO();
        ArrayList<SustrateNetworkEntity> redesFisicas
                = new ArrayList<>(networkDAO.getAll(entityManager));
        ArrayList<SustrateNetwork> redesSustratos = new ArrayList<>();

        redesFisicas.forEach((redFisica) -> {
            SustrateNetwork redObtenida = new SustrateNetwork();
            redObtenida.setNombre(redFisica.getNombre());
            redObtenida.setMinCPU(redFisica.getMinCPU());
            redObtenida.setMaxCPU(redFisica.getMaxCPU());
            redObtenida.setFecha(redFisica.getFecha());

            ArrayList<SustrateNode> nodos = new ArrayList<>();

            networkDAO.nodosPorRedFisica(redFisica,
                    entityManager).forEach((nodoEntity) -> {
                        SustrateNode nodo = new SustrateNode();
                        nodo.setNombre(nodoEntity.getNombre());
                        nodo.setIdentificador(nodoEntity.getIdentificador());
                        nodo.setCapacidadCPU(nodoEntity.getCapacidadCPU());
                        nodos.add(nodo);
                    });

            Collections.sort(nodos, new Comparator<SustrateNode>() {
                @Override
                public int compare(SustrateNode o1, SustrateNode o2) {
                    return new Integer(o1.getIdentificador()).
                            compareTo(o2.getIdentificador());
                }
            });

            redObtenida.setNodosFisicos(nodos);
            ArrayList<SustrateEdge> enlaces = new ArrayList<>();

            networkDAO.enlacesPorRedFisica(redFisica, entityManager).
                    forEach((enlaceEntity) -> {
                        SustrateEdge enlace = new SustrateEdge();
                        enlace.setIdentificador(enlaceEntity.
                                getIdentificador());
                        enlace.setNombre(enlaceEntity.getNombre());
                        enlace.setDistancia(enlaceEntity.getDistancia());
                        enlace.setCantidadFS(enlaceEntity.getCantidadFS());
                        SustrateNode nodoUno = null, nodoDos = null;

                        for (SustrateNode nodo : nodos) {
                            if (nodo.getIdentificador() == enlaceEntity.
                                    getNodoUno().getIdentificador()) {
                                nodoUno = nodo;
                            } else if (nodo.getIdentificador() == enlaceEntity.
                                    getNodoDos().getIdentificador()) {
                                nodoDos = nodo;
                            }

                            if (nodoUno != null && nodoDos != null) {
                                break;
                            }
                        }
                        enlace.setNodoUno(nodoUno);
                        enlace.setNodoDos(nodoDos);
                        nodoUno.agregarEnlaceAdyacente(enlace);
                        nodoDos.agregarEnlaceAdyacente(enlace);
                        enlaces.add(enlace);
                    });
            redObtenida.setEnlacesFisicos(enlaces);
            redesSustratos.add(redObtenida);
        });
        return redesSustratos;
    }

    /**
     * Metodo que se encarga de traer todos los conjuntos de redes virtuales
     * (tanto estaticas como dinamicas) almacenados en la BD.
     *
     * @param entityManager instancia para poder conectarse a la BD.
     * @param esEstatico boolean que define si quiere que retorne todos los
     * conjuntos estaticos (true) o dinamicos (false).
     * @return todos los conjuntos de redes virtuales que estan guardadas en la
     * BD en base a lo solicitado (si quiere dinamicas o estaticas).
     */
    public static ArrayList<VirtualNetworkList>
            obtenerTotalConjuntosRedesVirtuales(EntityManager entityManager,
                    boolean esEstatico) {
        VirtualNetworkListDAO networkListDAO = new VirtualNetworkListDAO();
        VirtualNetworkDAO networkDAO = new VirtualNetworkDAO();
        ArrayList<VirtualNetworkListEntity> conjuntoRedesVirtuales
                = networkListDAO.obtenerRedesVirtualesPorTipo(esEstatico,
                        entityManager);
        ArrayList<VirtualNetworkList> redesVirtuales = new ArrayList<>();

        conjuntoRedesVirtuales.forEach((vNetEntitySet) -> {
            /*Aqui se inicia creando la clase que almacena un conjunto de redes 
             *virtuales que se trae de la BD
             */
            VirtualNetworkList vNetSet = new VirtualNetworkList();
            vNetSet.setNombre(vNetEntitySet.getNombre());
            vNetSet.setEsEstatico(vNetEntitySet.isEsEstatico());
            vNetSet.setCiclos(vNetEntitySet.getCiclos());
            vNetSet.setErlangs(vNetEntitySet.getErlangs());
            vNetSet.setHoldingTime(vNetEntitySet.getHoldingTime());
            vNetSet.setLambda(vNetEntitySet.getLambda());
            vNetSet.setMinCPU(vNetEntitySet.getMinCPU());
            vNetSet.setMaxCPU(vNetEntitySet.getMaxCPU());
            vNetSet.setMinFS(vNetEntitySet.getMinFS());
            vNetSet.setMaxFS(vNetEntitySet.getMaxFS());
            vNetSet.setFecha(vNetEntitySet.getFecha());
            vNetSet.setMinNumeroNodos(vNetEntitySet.getMinNumeroNodos());
            vNetSet.setMaxNumeroNodos(vNetEntitySet.getMaxNumeroNodos());
            vNetSet.setCantidadRedes(vNetEntitySet.getCantidadRedes());

            ArrayList<VirtualNetworkEntity> networkEntitys
                    = networkListDAO.obtenerRedesAsociadas(vNetEntitySet,
                            entityManager);

            ArrayList<VirtualNetwork> networks = new ArrayList<>();
            networkEntitys.forEach((networkEntity) -> {
                /*Aqui se crea una red virtual y luego se agrega al conjunto*/
                VirtualNetwork network = new VirtualNetwork();
                network.setIdentificador(networkEntity.getIdentificador());
                network.setNombre(networkEntity.getNombre());
                network.setMapeado(networkEntity.isMapeado());
                network.setTiempoInicial(networkEntity.getTiempoInicial());
                network.setTiempoFinal(networkEntity.getTiempoFinal());

                /*Se crea los nodos y luego se asignan a la red*/
                ArrayList<VirtualNode> nodos = new ArrayList<>();

                networkDAO.nodosPorRedVirtual(networkEntity,
                        entityManager).forEach((nodoEntity) -> {
                            VirtualNode nodo = new VirtualNode();
                            nodo.setIdentificador(nodoEntity.
                                    getIdentificador());
                            nodo.setNombre(nodoEntity.getNombre());
                            nodo.setCapacidadCPU(nodoEntity.getCapacidadCPU());
                            nodo.setMapeado(nodoEntity.isMapeado());
                            nodos.add(nodo);
                        });

                Collections.sort(nodos, new Comparator<VirtualNode>() {
                    @Override
                    public int compare(VirtualNode o1, VirtualNode o2) {
                        return new Integer(o1.getIdentificador()).
                                compareTo(o2.getIdentificador());
                    }
                });
                network.setNodosVirtuales(nodos);

                /*Se crean los enlaces y se relacionan con su la red y con sus 
                 *respectivos nodoUno y nodoDos
                 */
                ArrayList<VirtualEdge> enlaces = new ArrayList<>();

                networkDAO.enlacesPorRedVirtual(networkEntity,
                        entityManager).forEach((enlaceEntity) -> {
                            VirtualEdge enlace = new VirtualEdge();
                            enlace.setIdentificador(enlaceEntity.
                                    getIdentificador());
                            enlace.setNombre(enlaceEntity.getNombre());
                            enlace.setCantidadFS(enlaceEntity.getCantidadFS());
                            enlace.setMapeado(enlaceEntity.isMapeado());

                            VirtualNode nodoUno = null, nodoDos = null;

                            for (VirtualNode nodo : nodos) {
                                if (nodo.getIdentificador() == enlaceEntity.
                                        getNodoUno().getIdentificador()) {
                                    nodoUno = nodo;
                                } else if (nodo.getIdentificador() == enlaceEntity.
                                        getNodoDos().getIdentificador()) {
                                    nodoDos = nodo;
                                }

                                if (nodoUno != null && nodoDos != null) {
                                    break;
                                }
                            }
                            enlace.setNodoUno(nodoUno);
                            enlace.setNodoDos(nodoDos);
                            nodoUno.agregarEnlaceVirtual(enlace);
                            nodoDos.agregarEnlaceVirtual(enlace);
                            enlaces.add(enlace);
                        });
                network.setEnlacesVirtuales(enlaces);
                networks.add(network);
            });
            vNetSet.setConjuntoRedes(networks);
            redesVirtuales.add(vNetSet);
        });
        return redesVirtuales;
    }

    /**
     * Metodo para obtener la red física existente seleccionada
     *
     * @param nombreRed Nombre de red a ser obtenida
     * @return red física seleccionada
     *
     * TODO Ver si se puede optimizar luego
     */
    public static SustrateNetwork obtenerRedFisica(String nombreRed) {

        SustrateNetworkDAO networkDAO = new SustrateNetworkDAO();
        ArrayList<SustrateNetworkEntity> redesFisicas = new ArrayList<>(networkDAO.getAll(Main.entityManager));
        SustrateNetwork redObtenida = new SustrateNetwork();

        redesFisicas.forEach((redFisica) -> {

            // Si la red fisica es igual a la red seleccionada obtenemos los datos
            if (redFisica.getNombre().equals(nombreRed)) {

                redObtenida.setNombre(redFisica.getNombre());
                redObtenida.setFecha(redFisica.getFecha());
                redObtenida.setMinCPU(redFisica.getMinCPU());
                redObtenida.setMaxCPU(redFisica.getMaxCPU());
                redObtenida.setCantidadFS(redFisica.getCantidadFS());

                ArrayList<SustrateNode> nodos = new ArrayList<>();
                ArrayList<SustrateEdge> enlaces = new ArrayList<>();

                // Se obtienen los nodos de la red
                networkDAO.nodosPorRedFisica(redFisica, Main.entityManager).forEach((nodoEntity) -> {
                    SustrateNode nodo = new SustrateNode();
                    nodo.setNombre(nodoEntity.getNombre());
                    nodo.setIdentificador(nodoEntity.getIdentificador());
                    nodo.setCapacidadCPU(nodoEntity.getCapacidadCPU());
                    nodos.add(nodo);
                });

                Collections.sort(nodos, new Comparator<SustrateNode>() {
                    @Override
                    public int compare(SustrateNode o1, SustrateNode o2) {
                        return new Integer(o1.getIdentificador()).compareTo(o2.getIdentificador());
                    }
                });

                networkDAO.enlacesPorRedFisica(redFisica, Main.entityManager).
                        forEach((enlaceEntity) -> {

                            SustrateEdge enlace = new SustrateEdge();
                            enlace.setIdentificador(enlaceEntity.
                                    getIdentificador());
                            enlace.setNombre(enlaceEntity.getNombre());
                            enlace.setDistancia(enlaceEntity.getDistancia());
                            enlace.setCantidadFS(enlaceEntity.getCantidadFS());

                            SustrateNode nodoUno = null, nodoDos = null;
                            for (SustrateNode nodo : nodos) {
                                if (nodo.getIdentificador() == enlaceEntity.
                                        getNodoUno().getIdentificador()) {
                                    nodoUno = nodo;
                                } else if (nodo.getIdentificador() == enlaceEntity.
                                        getNodoDos().getIdentificador()) {
                                    nodoDos = nodo;
                                }
                                if (nodoUno != null && nodoDos != null) {
                                    break;
                                }
                            }

                            enlace.setNodoUno(nodoUno);
                            enlace.setNodoDos(nodoDos);
                            nodoUno.agregarEnlaceAdyacente(enlace);
                            nodoDos.agregarEnlaceAdyacente(enlace);
                            enlaces.add(enlace);
                        });
                redObtenida.setEnlacesFisicos(enlaces);
                redObtenida.setNodosFisicos(nodos);
            }
        });
        return redObtenida;
    }

    public static ArrayList<VirtualNetworkListEntity> obtenerListaConjuntoRedesVirtuales(
            EntityManager entityManager, boolean esEstatico) {

        VirtualNetworkListDAO networkListDAO = new VirtualNetworkListDAO();

        return networkListDAO.obtenerRedesVirtualesPorTipo(esEstatico,
                entityManager);
    }

    public static VirtualNetworkList obtenerConjuntoRedesVirtuales(
            String nombreConjunto, EntityManager entityManager,
            boolean esEstatico) {
        VirtualNetworkListEntity conjuntoSeleccionado = null;
        VirtualNetworkListDAO networkListDAO = new VirtualNetworkListDAO();
        VirtualNetworkDAO networkDAO = new VirtualNetworkDAO();

        for (VirtualNetworkListEntity conjunto
                : networkListDAO.obtenerRedesVirtualesPorTipo(esEstatico,
                        entityManager)) {
            if (conjunto.getNombre().equals(nombreConjunto)) {
                conjuntoSeleccionado = conjunto;
                break;
            }
        }

        /* Aqui se inicia creando la clase que almacena un conjunto de redes 
         * virtuales que se trae de la BD
         */
        VirtualNetworkList conjuntoRedVirtual = new VirtualNetworkList();
        conjuntoRedVirtual.setNombre(conjuntoSeleccionado.getNombre());
        conjuntoRedVirtual.setEsEstatico(conjuntoSeleccionado.isEsEstatico());
        conjuntoRedVirtual.setCiclos(conjuntoSeleccionado.getCiclos());
        conjuntoRedVirtual.setErlangs(conjuntoSeleccionado.getErlangs());
        conjuntoRedVirtual.setHoldingTime(conjuntoSeleccionado.getHoldingTime());
        conjuntoRedVirtual.setLambda(conjuntoSeleccionado.getLambda());
        conjuntoRedVirtual.setMinCPU(conjuntoSeleccionado.getMinCPU());
        conjuntoRedVirtual.setMaxCPU(conjuntoSeleccionado.getMaxCPU());
        conjuntoRedVirtual.setMinFS(conjuntoSeleccionado.getMinFS());
        conjuntoRedVirtual.setMaxFS(conjuntoSeleccionado.getMaxFS());
        conjuntoRedVirtual.setFecha(conjuntoSeleccionado.getFecha());
        conjuntoRedVirtual.setMinNumeroNodos(conjuntoSeleccionado.getMinNumeroNodos());
        conjuntoRedVirtual.setMaxNumeroNodos(conjuntoSeleccionado.getMaxNumeroNodos());
        conjuntoRedVirtual.setCantidadRedes(conjuntoSeleccionado.getCantidadRedes());

        ArrayList<VirtualNetworkEntity> networkEntitys
                = networkListDAO.obtenerRedesAsociadas(conjuntoSeleccionado,
                        entityManager);

        ArrayList<VirtualNetwork> networks = new ArrayList<>();
        networkEntitys.forEach((networkEntity) -> {
            /*Aqui se crea una red virtual y luego se agrega al conjunto*/
            VirtualNetwork network = new VirtualNetwork();
            network.setIdentificador(networkEntity.getIdentificador());
            network.setNombre(networkEntity.getNombre());
            network.setMapeado(networkEntity.isMapeado());
            network.setTiempoInicial(networkEntity.getTiempoInicial());
            network.setTiempoFinal(networkEntity.getTiempoFinal());

            /*Se crea los nodos y luego se asignan a la red*/
            ArrayList<VirtualNode> nodos = new ArrayList<>();

            networkDAO.nodosPorRedVirtual(networkEntity,
                    entityManager).forEach((nodoEntity) -> {
                        VirtualNode nodo = new VirtualNode();
                        nodo.setIdentificador(nodoEntity.
                                getIdentificador());
                        nodo.setNombre(nodoEntity.getNombre());
                        nodo.setCapacidadCPU(nodoEntity.getCapacidadCPU());
                        nodo.setMapeado(nodoEntity.isMapeado());
                        nodos.add(nodo);
                    });

            Collections.sort(nodos, new Comparator<VirtualNode>() {
                @Override
                public int compare(VirtualNode o1, VirtualNode o2) {
                    return new Integer(o1.getIdentificador()).
                            compareTo(o2.getIdentificador());
                }
            });
            network.setNodosVirtuales(nodos);

            /*Se crean los enlaces y se relacionan con su la red y con sus 
                 *respectivos nodoUno y nodoDos
             */
            ArrayList<VirtualEdge> enlaces = new ArrayList<>();

            networkDAO.enlacesPorRedVirtual(networkEntity,
                    entityManager).forEach((enlaceEntity) -> {
                        VirtualEdge enlace = new VirtualEdge();
                        enlace.setIdentificador(enlaceEntity.
                                getIdentificador());
                        enlace.setNombre(enlaceEntity.getNombre());
                        enlace.setCantidadFS(enlaceEntity.getCantidadFS());
                        enlace.setMapeado(enlaceEntity.isMapeado());

                        VirtualNode nodoUno = null, nodoDos = null;

                        for (VirtualNode nodo : nodos) {
                            if (nodo.getIdentificador() == enlaceEntity.
                                    getNodoUno().getIdentificador()) {
                                nodoUno = nodo;
                            } else if (nodo.getIdentificador() == enlaceEntity.
                                    getNodoDos().getIdentificador()) {
                                nodoDos = nodo;
                            }

                            if (nodoUno != null && nodoDos != null) {
                                break;
                            }
                        }
                        enlace.setNodoUno(nodoUno);
                        enlace.setNodoDos(nodoDos);
                        nodoUno.agregarEnlaceVirtual(enlace);
                        nodoDos.agregarEnlaceVirtual(enlace);
                        enlaces.add(enlace);
                    });
            network.setEnlacesVirtuales(enlaces);
            networks.add(network);
        });
        conjuntoRedVirtual.setConjuntoRedes(networks);
        return conjuntoRedVirtual;
    }
}
