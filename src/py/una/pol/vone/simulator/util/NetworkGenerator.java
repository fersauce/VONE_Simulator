package py.una.pol.vone.simulator.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import py.una.pol.vone.simulator.DAO.SustrateNetworkDAO;
import py.una.pol.vone.simulator.DAO.VirtualNetworkListDAO;

import py.una.pol.vone.simulator.model.SustrateEdge;
import py.una.pol.vone.simulator.model.SustrateNetwork;
import py.una.pol.vone.simulator.model.SustrateNode;
import py.una.pol.vone.simulator.model.VirtualEdge;
import py.una.pol.vone.simulator.model.VirtualNetwork;
import py.una.pol.vone.simulator.model.VirtualNetworkList;
import py.una.pol.vone.simulator.model.VirtualNode;

/**
 * Clase utilizada para generar las topologias de redes virtuales.
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.0
 * @since 2017-03-04
 */
public class NetworkGenerator {

    /**
     * Metodo utilizado para generar las redes virtuales a utilizar.
     *
     * @param cantidadRedes numero de requerimientos de red.
     * @param minNodos numero minimo de nodos por red.
     * @param maxNodos numero maximo de nodos por red.
     * @param requerimientosVirtuales Listado en donde se van a almacenar las
     * redes generadas.
     */
    public static void generarRedesVirtuales(int cantidadRedes, int minNodos,
            int maxNodos, ArrayList<VirtualNetwork> requerimientosVirtuales) {
        ArrayList<Graph<Object, DefaultEdge>> topologias = TopologyGenerator.
                generarRedes(cantidadRedes, minNodos, maxNodos);
        VirtualNetwork redVirtual;
        ArrayList<VirtualNode> nodosVirtuales = null;
        ArrayList<VirtualEdge> enlacesVirtuales = null;
        Set<Object> nodos;
        Set<DefaultEdge> enlaces;
        VirtualNode nodoNuevo = null, nodoOrigen = null, nodoDestino = null;
        VirtualEdge enlaceNuevo = null;
        DefaultEdge arista = null;
        for (Graph<Object, DefaultEdge> topologia : topologias) {
            redVirtual = new VirtualNetwork();
            nodosVirtuales = new ArrayList<>();
            enlacesVirtuales = new ArrayList<>();
            nodos = topologia.vertexSet();
            enlaces = topologia.edgeSet();
            /*Se cargan los nodos virtuales*/
            for (Object nodo : nodos) {
                nodoNuevo = new VirtualNode();
                nodoNuevo.setIdentificador((int) nodo);
                nodosVirtuales.add(nodoNuevo);
                nodoNuevo = null;
            }
            /*Se cargan los enlaces virtuales*/
            Iterator<DefaultEdge> iteradorEnlaces = enlaces.iterator();
            while (iteradorEnlaces.hasNext()) {
                arista = iteradorEnlaces.next();
                for (VirtualNode nodo : nodosVirtuales) {
                    if (nodo.getIdentificador()
                            == Integer.valueOf(topologia.getEdgeSource(arista).
                                    toString())) {
                        nodoOrigen = nodo;
                    } else if (nodo.getIdentificador()
                            == Integer.valueOf(topologia.getEdgeTarget(arista).
                                    toString())) {
                        nodoDestino = nodo;
                    }
                    if (nodoOrigen != null && nodoDestino != null) {
                        break;
                    }
                }
                enlaceNuevo = new VirtualEdge();
                enlaceNuevo.setNodoUno(nodoOrigen);
                enlaceNuevo.setNodoDos(nodoDestino);
                ArrayList<VirtualEdge> adyacentesOrigen = nodoOrigen.
                        getAdyacentes();
                adyacentesOrigen.add(enlaceNuevo);
                nodoOrigen.setAdyacentes(adyacentesOrigen);
                ArrayList<VirtualEdge> adyacentesDestino = nodoDestino.
                        getAdyacentes();
                adyacentesDestino.add(enlaceNuevo);
                nodoDestino.setAdyacentes(adyacentesDestino);
                enlacesVirtuales.add(enlaceNuevo);
                nodoOrigen = null;
                nodoDestino = null;
                enlaceNuevo = null;
            }
            /*Se genera la red virtual y se carga a la lista de redes y 
             *se liberan los recursos
             */
            redVirtual.setNodosVirtuales(nodosVirtuales);
            redVirtual.setEnlacesVirtuales(enlacesVirtuales);
            requerimientosVirtuales.add(redVirtual);
            redVirtual = null;
            nodosVirtuales = null;
            enlacesVirtuales = null;
            Runtime.getRuntime().gc();
        }
    }

    /**
     * Metodo para generar el esqueleto de la red fisica.
     *
     * @param redFisica objeto que va a almacenar la red fisica.
     * @param pathRed direccion de donde obtener los enlaces de la red fisica.
     * @param cantidadNodos numero de nodos que va a contener la red fisica.
     */
    public static void generarRedFisica(SustrateNetwork redFisica,
            String pathRed, int cantidadNodos) {
        ArrayList<SustrateNode> nodosFisicos = new ArrayList<>();
        ArrayList<SustrateEdge> enlacesFisicos = new ArrayList<>();
        //Esta parte es para generar los nodos
        SustrateNode nodoNuevo;
        for (int i = 0; i < cantidadNodos; i++) {
            //Se hace el random entre 10 y 20 unidades de CPU.
            nodoNuevo = new SustrateNode();
            nodoNuevo.setIdentificador(i);
            nodoNuevo.setNombre("Nodo" + nodoNuevo.getIdentificador());
            nodosFisicos.add(nodoNuevo);
            nodoNuevo = null;
        }
        Collections.sort(nodosFisicos, new Comparator<SustrateNode>() {
            @Override
            public int compare(SustrateNode o1, SustrateNode o2) {
                return new Integer(o1.getIdentificador()).
                        compareTo(o2.getIdentificador());
            }
        });

        //Metodo para generar los enlaces fisicos
        try {
            @SuppressWarnings("resource")
            BufferedReader bf = new BufferedReader(new FileReader(pathRed));
            String linea = bf.readLine();
            SustrateNode nodoOrigen = null, nodoDestino = null;
            SustrateEdge enlaceNuevo;
            boolean origenEncontrado = false, destinoEncontrado = false;
            int distancia;
            while (linea != null) {
                String[] descripcionEnlace = linea.split("\\s");
                if (descripcionEnlace.length == 3) {
                    for (SustrateNode nodo : nodosFisicos) {
                        if (nodo.getIdentificador()
                                == Integer.valueOf(descripcionEnlace[0])) {
                            nodoOrigen = nodo;
                            origenEncontrado = true;
                        } else if (nodo.getIdentificador()
                                == Integer.valueOf(descripcionEnlace[1])) {
                            nodoDestino = nodo;
                            destinoEncontrado = true;
                        }
                        if (origenEncontrado && destinoEncontrado) {
                            break;
                        }
                    }

                    /*La distancia es un valor que agregue, 
                    entre 1 y 5 km, por si los lleguemos a utilizar.*/
                    distancia = Integer.valueOf(descripcionEnlace[2]);
                    /*Las dos siguientes lineas es para verificar que no exista 
                     *el enlace, ya que el archivo tiene dos enlaces por el 
                     *tema de convertir en no dirigido dicho grafo
                     */

                    //Se registra el nuevo enlace con los nodos hallados.
                    enlaceNuevo = new SustrateEdge();
                    enlaceNuevo.setDistancia(distancia);
                    enlaceNuevo.setNombre("ENLACE"
                            + nodoOrigen.getIdentificador() + "A"
                            + nodoDestino.getIdentificador());
                    enlaceNuevo.setNodoUno(nodoOrigen);
                    enlaceNuevo.setNodoDos(nodoDestino);
                    ArrayList<SustrateEdge> adyacentesOrigen
                            = nodoOrigen.getAdyacentes();
                    adyacentesOrigen.add(enlaceNuevo);
                    nodoOrigen.setAdyacentes(adyacentesOrigen);
                    ArrayList<SustrateEdge> adyacentesDestino
                            = nodoDestino.getAdyacentes();
                    adyacentesDestino.add(enlaceNuevo);
                    nodoDestino.setAdyacentes(adyacentesDestino);
                    enlacesFisicos.add(enlaceNuevo);
                    origenEncontrado = false;
                    destinoEncontrado = false;
                    nodoOrigen = null;
                    nodoDestino = null;
                    enlaceNuevo = null;

                }
                linea = bf.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        redFisica.setEnlacesFisicos(enlacesFisicos);
        redFisica.setNodosFisicos(nodosFisicos);
    }

    /**
     * Metodo que genera la cantidad de redes a cargar en cada iteracion del
     * VONE dinamico
     *
     * @param lambda atributo que almacena el valor de lambda para la prueba
     * seleccionada.
     * @return Numero de redes a ingresar en la iteracion que lo ha llamado.
     */
    private static int numeroDeRedesAIngresar(double lambda) {
        double tope, actual;
        tope = doubleAleatorio();
        int B = 0;
        try {
            actual = Math.exp(-lambda)
                    * (Math.pow(lambda, (double) B) / (double) factorial(B));
            while (tope > actual) {
                B++;
                actual = actual + Math.exp(-lambda)
                        * (Math.pow(lambda, (double) B) / (double) factorial(B));
            }
        } catch (Exception ex) {
            Logger.getLogger(NetworkGenerator.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        return B;
    }

    /**
     * Retorna el valor factorial de un numero.
     *
     * @param valor numero del cual se quiere hallar el factorial.
     * @return el factorial del valor pasado como parametro.
     * @exception si el numero pasado es menor a cero.
     */
    private static int factorial(int valor) throws Exception {
        int valorFactorial = 1;
        if (valor < 0) {
            throw new Exception("Se ha ingresado un numero negativo");
        }
        if (valor > 1) {
            for (int numero = 2; numero <= valor; numero++) {
                valorFactorial *= numero;
            }
        }
        return valorFactorial;
    }

    /**
     * Metodo que retorna un double entre 0.0 y 1.0 (incluyendo ambos topes).
     *
     * @return double aleatorio entre [0.0,1.0]
     */
    private static double doubleAleatorio() {
        Random generador = new Random();
        /* Se hace un boolean aleatorio y si el resultado es true, genera un 
         * double aleatorio entre [0.0,1.0); y se da false genera un numero 
         * double aleatorio entre (0.0,1.0]
         */
        if (generador.nextBoolean()) {
            return generador.nextDouble();
        } else {
            return 1.0 - generador.nextDouble();
        }
    }

    /**
     * Metodo que halla el tiempo de vida del requerimiento que lo solicita.
     *
     * @param holdingTime tiempo de vida promedio.
     * @return tiempo de vida del requerimiento que lo solicita.
     */
    private static int tiempoDeVida(int holdingTime) {
        double tope = doubleAleatorio();
        int B = 1;
        while ((1.0 - Math.exp((double) -B / (double) holdingTime)) < tope) {
            B++;
        }
        return B;
    }

    /**
     * Metodo que halla los requerimientos de redes virtuales para el caso
     * dinamico.
     *
     * @param ciclos numero de ciclos que se realizara el caso dinamico.
     * @param erlangs trafico de la red.
     * @param holdingTime tiempo promedio de vida.
     * @param minimoCPU valor minimo del rango de CPU a tener cada nodo virtual.
     * @param maximoCPU valor maximo del rango de CPU a tener cada nodo virtual.
     * @param minimoFS valor minimo del rango de FS a tener cada enlace virtual.
     * @param maximoFS valor maximo del rango de FS a tener cada enlace virtual.
     * @param minimoNodos valor minimo del rango de cantidad de nodos a tener
     * cada red virtual.
     * @param maximoNodos valor maximo del rango de cantidad de nodos a tener
     * cada red virtual.
     * @param entityManager instacia para conectar a la BD.
     * @return conjunto de requerimientos virtuales generados dinamicamente con
     * esos valores, ordenados por tiempo de ingreso (de menor a mayor ciclo de
     * ingreso) y el segundo criterio es por tiempo de salida (de menor a mayor
     * tiempo de salida).
     */
    public static VirtualNetworkList generarRequerimientosDinamicos(int ciclos,
            int erlangs, int holdingTime, int minimoCPU, int maximoCPU,
            int minimoFS, int maximoFS, int minimoNodos, int maximoNodos,
            EntityManager entityManager) {
        VirtualNetworkList nuevo = new VirtualNetworkList();
        VirtualNetworkListDAO networkListDAO = new VirtualNetworkListDAO();
        nuevo.setNombre("Conjunto" + (networkListDAO.getAll(entityManager).
                size() + 1));
        nuevo.setCiclos(ciclos);
        nuevo.setErlangs(erlangs);
        nuevo.setEsEstatico(false);
        nuevo.setHoldingTime(holdingTime);
        nuevo.setLambda((double) erlangs / (double) holdingTime);
        nuevo.setMinCPU(minimoCPU);
        nuevo.setMaxCPU(maximoCPU);
        nuevo.setMinFS(minimoFS);
        nuevo.setMaxFS(maximoFS);
        nuevo.setMinNumeroNodos(minimoNodos);
        nuevo.setMaxNumeroNodos(maximoNodos);
        ArrayList<VirtualNetwork> redes = new ArrayList<>();
        int idRed = 1;
        for (int ciclo = 1; ciclo <= ciclos; ciclo++) {
            int redesAIngresar = numeroDeRedesAIngresar(nuevo.getLambda());
            if (redesAIngresar > 0) {
                ArrayList<VirtualNetwork> ingresar = new ArrayList<>();
                generarRedesVirtuales(redesAIngresar, minimoNodos, maximoNodos,
                        ingresar);
                for (VirtualNetwork nuevaRed : ingresar) {
                    nuevaRed.setTiempoInicial(ciclo);
                    int tiempoDeVida = tiempoDeVida(nuevo.
                            getHoldingTime()) + ciclo - 1;
                    nuevaRed.setTiempoFinal(tiempoDeVida);
                }
                Collections.sort(ingresar, new Comparator<VirtualNetwork>() {
                    @Override
                    public int compare(VirtualNetwork o1, VirtualNetwork o2) {
                        return new Integer(o1.getTiempoInicial()).
                                compareTo(o2.getTiempoInicial());
                    }
                });
                for (VirtualNetwork nuevaRed : ingresar) {
                    nuevaRed.setNombre("VNR" + idRed);
                    nuevaRed.setIdentificador(idRed++);
                    nuevaRed.getNodosVirtuales().forEach((nodo) -> {
                        nodo.setCapacidadCPU(TopologyGenerator.
                                generarNumeroEnteroAleatorioConRango(minimoCPU,
                                        maximoCPU));
                        nodo.setNombre("VNR" + nuevaRed.getIdentificador()
                                + "Nodo" + nodo.getIdentificador());
                    });
                    nuevaRed.getEnlacesVirtuales().forEach((enlace) -> {
                        enlace.setCantidadFS(TopologyGenerator.
                                generarNumeroEnteroAleatorioConRango(minimoFS,
                                        maximoFS));
                        enlace.setNombre("VNR" + nuevaRed.getIdentificador()
                                + "E" + enlace.getNodoUno().getIdentificador()
                                + "A" + enlace.getNodoDos().getIdentificador());
                    });
                    redes.add(nuevaRed);
                }
            }
        }
        nuevo.setCantidadRedes(redes.size());
        nuevo.setConjuntoRedes(redes);
        return nuevo;
    }

    /**
     * Metodo que halla los requerimientos de redes virtuales del caso estatico
     * aleatorio y manual.
     *
     * @param cantidadRedes numero de redes a contener el conjunto.
     * @param minimoCPU valor minimo del rango de CPU a tener cada nodo virtual.
     * @param maximoCPU valor maximo del rango de CPU a tener cada nodo virtual.
     * @param minimoFS valor minimo del rango de FS a tener cada enlace virtual.
     * @param maximoFS valor maximo del rango de FS a tener cada enlace virtual.
     * @param minimoNodos valor minimo del rango de cantidad de nodos a tener
     * cada red virtual.
     * @param maximoNodos valor maximo del rango de cantidad de nodos a tener
     * cada red virtual.
     * @param entityManager instacia para conectar a la BD.
     * @return conjunto de requerimientos virtuales generados dinamicamente con
     * esos valores, ordenados por capacidad total de CPU por requerimiento (de
     * mayor a menor).
     */
    public static VirtualNetworkList
            generarRequerimientosEstaticos(int cantidadRedes, int minimoCPU,
                    int maximoCPU, int minimoFS, int maximoFS, int minimoNodos,
                    int maximoNodos, EntityManager entityManager) {

        VirtualNetworkList nuevo = new VirtualNetworkList();
        VirtualNetworkListDAO networkListDAO = new VirtualNetworkListDAO();
        ArrayList<VirtualNetwork> redes = new ArrayList<>();
        int idRed = 1;

        nuevo.setNombre("Conjunto" + (networkListDAO.getAll(entityManager).
                size() + 1));
        nuevo.setCiclos(0);
        nuevo.setErlangs(0);
        nuevo.setEsEstatico(true);
        nuevo.setHoldingTime(0);
        nuevo.setLambda(0.0);
        nuevo.setCantidadRedes(cantidadRedes);
        nuevo.setFecha(new Date());
        nuevo.setMinNumeroNodos(minimoNodos);
        nuevo.setMaxNumeroNodos(maximoNodos);

        /*Se generan los esqueletos*/
        generarRedesVirtuales(cantidadRedes, minimoNodos, maximoNodos, redes);

        // Si es Estatico y carga aleatoria
        if (maximoCPU > 0) {
            nuevo.setMinCPU(minimoCPU);
            nuevo.setMaxCPU(maximoCPU);
            nuevo.setMinFS(minimoFS);
            nuevo.setMaxFS(maximoFS);

            /*Se cargan las CPUs y FSs de manera aleatorio segun el rango pasado*/
            redes.forEach((red) -> {
                red.getNodosVirtuales().forEach((nodo) -> {
                    nodo.setCapacidadCPU(TopologyGenerator.
                            generarNumeroEnteroAleatorioConRango(minimoCPU,
                                    maximoCPU));
                });
                red.getEnlacesVirtuales().forEach((enlace) -> {
                    enlace.setCantidadFS(TopologyGenerator.
                            generarNumeroEnteroAleatorioConRango(minimoFS,
                                    maximoFS));
                });
            });

        } // Si es Estatico y carga manual
        else {
            nuevo.setMinCPU(0);
            nuevo.setMaxCPU(0);
            nuevo.setMinFS(0);
            nuevo.setMaxFS(0);
        }

        /*Se ordena de mayor a menor en base a la cantidad de CPU de cada uno, 
         *se asigna su id y nombre y se pasa
         */
        Collections.sort(redes, new Comparator<VirtualNetwork>() {
            @Override
            public int compare(VirtualNetwork o1, VirtualNetwork o2) {
                return new Integer(o2.getTotalCPU()).
                        compareTo(o1.getTotalCPU());
            }
        });

        for (VirtualNetwork red : redes) {
            red.setIdentificador(idRed++);
            red.setNombre("VNR" + red.getIdentificador());
            red.getNodosVirtuales().forEach((nodo) -> {
                nodo.setNombre("VNR" + red.getIdentificador()
                        + "Nodo" + nodo.getIdentificador());
            });
            red.getEnlacesVirtuales().forEach((enlace) -> {
                enlace.setNombre("VNR" + red.getIdentificador()
                        + "E" + enlace.getNodoUno().getIdentificador()
                        + "A" + enlace.getNodoDos().getIdentificador());
            });
        }
        nuevo.setConjuntoRedes(redes);
        return nuevo;
    }

    /**
     * Meotodo que generar el requerimiento de red fisica en base a lo
     * solicitado
     *
     * @param cantidadNodos numero de nodos que va a tener la red
     * @param pathRed ruta del archivo en donde se almacenan los enlaces
     * @param minimoCPU valor minimo del rango de CPU a tener cada nodo fisico.
     * @param maximoCPU valor maximo del rango de CPU a tener cada nodo fisico.
     * @param cantidadFS numero de FS a tener cada enlace fisico.
     * @param nombreRed nombre de la red fisica seleccionada.
     * @param entityManager instancia para conectarse a la BD.
     * @return red sustrato inicializada y configurada con los parametros de
     * entrada que han solicitado.
     */
    public static SustrateNetwork generarRedFisicaAutomatico(int cantidadNodos,
            String pathRed, int minimoCPU, int maximoCPU, int cantidadFS,
            String nombreRed, EntityManager entityManager) {
        SustrateNetwork nuevo = new SustrateNetwork();
        SustrateNetworkDAO networkDAO = new SustrateNetworkDAO();
        generarRedFisica(nuevo, pathRed, cantidadNodos);
        nuevo.setNombre("RedFisica" + (networkDAO.
                getAll(entityManager).size() + 1) + nombreRed);
        nuevo.setMinCPU(minimoCPU);
        nuevo.setMaxCPU(maximoCPU);
        nuevo.setCantidadFS(cantidadFS);
        nuevo.getNodosFisicos().forEach((nodo) -> {
            nodo.setCapacidadCPU(TopologyGenerator.
                    generarNumeroEnteroAleatorioConRango(minimoCPU, maximoCPU));
        });
        nuevo.getEnlacesFisicos().forEach((enlace) -> {
            enlace.setCantidadFS(cantidadFS);
        });
        return nuevo;
    }

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("VONE_SimulatorPU");
        EntityManager em = emf.createEntityManager();
        SustrateNetwork redFisica = new SustrateNetwork();
        String pathRed = "static/sustatrenets/NSFNet.txt";
        ArrayList<VirtualNetwork> redesVirtuales = new ArrayList<>();
        //generarRedFisica(redFisica, pathRed, 13);
        //generarRedesVirtuales(4, 2, 7, redesVirtuales);
        /*System.out.println("Se imprime la red fisica");
        System.out.println("Nodos:");
        redFisica.getNodosFisicos().forEach((nodo) -> {
            System.out.println(nodo.getIdentificador());
        });
        System.out.println("Enlaces:");
        redFisica.getEnlacesFisicos().forEach((enlace) -> {
            System.out.println("Enlace entre " + enlace.getNodoUno().
                    getIdentificador() + " y " + enlace.getNodoDos().
                            getIdentificador());
        });
        System.out.println("Se persiste la red fisica en la BD.");
        //DataManagement.persistirRedFisica(redFisica, em);
        System.out.println("\n\nSe imprimen ahora las redes virtuales");
        int red = 1;
        for (VirtualNetwork redVirtual : redesVirtuales) {
            System.out.println("Red " + red++);
            System.out.println("Nodos:");
            redVirtual.getNodosVirtuales().forEach((nodo) -> {
                System.out.println(nodo.getIdentificador());
            });
            System.out.println("Enlaces:");
            redVirtual.getEnlacesVirtuales().forEach((enlace) -> {
                System.out.println("Enlace entre " + enlace.getNodoUno().
                        getIdentificador() + " y " + enlace.getNodoDos().
                                getIdentificador());
            });
        }
        VirtualNetworkList conjuntoGenerado = new VirtualNetworkList();
        conjuntoGenerado.setConjuntoRedes(redesVirtuales);
        DataManagement.persistirListaRedesVirtuales(conjuntoGenerado, em);*/
 /*ArrayList<SustrateNetwork> redesFisicasBD = DataManagement.
                obtenerTotalRedesFisicas(em);
        System.out.println("\n\nSe imprimen ahora las redes traidas de la BD");
        int redD = 1;
        for (SustrateNetwork redVirtual : redesFisicasBD) {
            System.out.println("Red " + redD++);
            System.out.println("Nodos:");
            redVirtual.getNodosFisicos().forEach((nodo) -> {
                System.out.println(nodo.getIdentificador());
            });
            System.out.println("La red cuenta con " + redVirtual.
                    getNodosFisicos().size() + " nodos");
            System.out.println("Enlaces:");
            redVirtual.getEnlacesFisicos().forEach((enlace) -> {
                System.out.println("Enlace entre " + enlace.getNodoUno().
                        getIdentificador() + " y " + enlace.getNodoDos().
                                getIdentificador());
            });
        }
        ArrayList<VirtualNetworkList> redesVirtualesBD = DataManagement.
                obtenerTotalConjuntosRedesVirtuales(em, false);
        int conjuntoVirtual = 1;
        for (VirtualNetworkList conjunto : redesVirtualesBD) {
            System.out.println("Conjunto de Red " + conjuntoVirtual++);
            System.out.println("Se imprimen las redes que contiene");
            int redVirt = 1;
            for (VirtualNetwork red : conjunto.getConjuntoRedes()) {
                System.out.println("Red " + redVirt++);
                System.out.println("Nodos:");
                red.getNodosVirtuales().forEach((nodo) -> {
                    System.out.println(nodo.getIdentificador());
                });
                System.out.println("Enlaces:");
                red.getEnlacesVirtuales().forEach((enlace) -> {
                    System.out.println("Enlace entre " + enlace.getNodoUno().
                            getIdentificador() + " y " + enlace.getNodoDos().
                                    getIdentificador());
                });
            }
        }*/
        //VirtualNetworkList nuevo = generarRequerimientosEstaticos(10, 2, 10, 2, 20, 2, 7, em);
        VirtualNetworkList nuevo = generarRequerimientosDinamicos(10, 7, 3, 2, 10, 2, 20, 2, 7, em);
        //DataManagement.persistirListaRedesVirtuales(nuevo, em);
        /*System.out.println("Conjunto de Red generado " + nuevo.getNombre());
        System.out.println("Se imprimen las redes que contiene");
        for (VirtualNetwork red : nuevo.getConjuntoRedes()) {
            System.out.println("Red: " + red.getIdentificador() + "\nIngreso ciclo: " + red.getTiempoInicial() + "\nSalida ciclo: " + red.getTiempoFinal());
            System.out.println("Nodos:");
            red.getNodosVirtuales().forEach((nodo) -> {
                System.out.println(nodo.getNombre() + " CPU: " + nodo.getCapacidadCPU());
            });
            System.out.println("Enlaces:");
            red.getEnlacesVirtuales().forEach((enlace) -> {
                System.out.println(enlace.getNombre() + " Cantidad FS: " + enlace.getCantidadFS());
            });
        }*/
        SustrateNetwork nuevaRedFisica = generarRedFisicaAutomatico(14, "static/sustatrenets/NSFNet.txt", 10, 20, 100, "NSFNET", em);
        /*System.out.println("Nodos:");
        nuevaRedFisica.getNodosFisicos().forEach((nodo) -> {
            System.out.println(nodo.getIdentificador());
            System.out.append("Enlaces Relacionados: ");
            nodo.getAdyacentes().forEach((enlaceAdyacente) -> {
                System.out.println(enlaceAdyacente.getNombre() + "; cantidad "
                        + "de FS: " + enlaceAdyacente.getCantidadFS());
            });
        });
        System.out.println("Enlaces:");
        nuevaRedFisica.getEnlacesFisicos().forEach((enlace) -> {
            System.out.println("Enlace entre " + enlace.getNodoUno().
                    getIdentificador() + " y " + enlace.getNodoDos().
                            getIdentificador());
        });*/
        ArrayList<String> metricasSeleccionadas = new ArrayList<>();
        metricasSeleccionadas.add("metrica uno");
        Simulations.ejecutarPruebaEstatica(nuevaRedFisica, nuevo, "Voraz Dinamico", metricasSeleccionadas);
    }
}
