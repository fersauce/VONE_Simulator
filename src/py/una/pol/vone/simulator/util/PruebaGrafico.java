package py.una.pol.vone.simulator.util;

import edu.uci.ics.jung.algorithms.layout.CircleLayout;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.visualization.VisualizationImageServer;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Stroke;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JFrame;
import org.apache.commons.collections15.Transformer;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

/**
 * Clase para probar JUNG
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 */
public class PruebaGrafico {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Graph<Object, DefaultEdge> grafo;
        grafo = TopologyGenerator.generarRedes(1, 2, 7).get(0);
        UndirectedSparseGraph grafoGrafico = new UndirectedSparseGraph();
        Set<Object> vertices = new HashSet<>();
        vertices.addAll(grafo.vertexSet());
        int counter = 1;
        for (Object vertex : vertices) {
            grafoGrafico.addVertex(vertex.toString());
            counter++;
        }
        Set enlaces = grafo.edgeSet();
        int enlaceContador = 1;
        for (DefaultEdge enlace : grafo.edgeSet()) {
            grafoGrafico.addEdge("Edge".concat(String.valueOf(enlaceContador)),
                    grafo.getEdgeSource(enlace).toString(),
                    grafo.getEdgeTarget(enlace).toString());
            enlaceContador++;
        }
        VisualizationImageServer vis = new VisualizationImageServer(
                new CircleLayout(grafoGrafico),
                new Dimension(1000, 1000));
        Transformer<Integer, Paint> verticesTransformer = new Transformer<Integer, Paint>() {
            @Override
            public Paint transform(Integer i) {
                return Color.DARK_GRAY;
            }
        };
        float dash[] = {10.0f};
        final Stroke enlaceStroke = new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);
        Transformer<String, Stroke> enlacesTransformer = new Transformer<String, Stroke>() {
            @Override
            public Stroke transform(String s) {
                return enlaceStroke;
            }
        };
        //vis.getRenderContext().setVertexFillPaintTransformer(verticesTransformer);
        vis.getRenderContext().setEdgeStrokeTransformer(enlacesTransformer);
        vis.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
        vis.getRenderContext().setEdgeLabelTransformer(new ToStringLabeller());
        vis.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.CNTR);

        JFrame frame = new JFrame();
        frame.getContentPane().add(vis);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
