package py.una.pol.vone.simulator.util;

import py.una.pol.vone.simulator.algorithms.Voraz;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import py.una.pol.vone.simulator.model.SustrateEdge;
import py.una.pol.vone.simulator.model.SustrateNetwork;
import py.una.pol.vone.simulator.model.SustrateNode;
import py.una.pol.vone.simulator.model.VirtualEdge;
import py.una.pol.vone.simulator.model.VirtualNetwork;
import py.una.pol.vone.simulator.model.VirtualNetworkList;
import py.una.pol.vone.simulator.model.VirtualNode;

/**
 * Clase en donde se ejecutaran las simulaciones; para las mismas se asumen que
 * las redes fisicas y virtuales que se pasan aqui ya se encuentran
 * inicializadas y generadas (utilizando los algoritmos de generacion de
 * {@link NetworkGenerator}).
 *
 * @author <a href="mailto: carlifer.fernando@gmail.com">Fernando Saucedo</a>
 * @version 1.1, 03/13/17
 */
public class Simulations {

    /**
     * Metodo que ejecuta la prueba estatica.
     *
     * @param redFisica red fisica generada.
     * @param conjuntoRedesVirtuales conjunto de redes virtuales generado.
     * @param algoritmoSeleccionado cadena que almacena el algoritmo
     * seleccionado por el usuario.
     * @param metricasSeleccionadas lista de metricas seleccionadas por el
     * usuario.
     * @return nombre del archivo generado o null.
     */
    public static String ejecutarPruebaEstatica(SustrateNetwork redFisica,
            VirtualNetworkList conjuntoRedesVirtuales,
            String algoritmoSeleccionado,
            ArrayList<String> metricasSeleccionadas) {
        long tiempoInicial = System.nanoTime();
        long inicioSimulador = tiempoInicial;
        try {
            ejecutarAlgoritmoSeleccionado((SustrateNetwork) redFisica.clone(),
                    conjuntoRedesVirtuales.getConjuntoRedes(),
                    algoritmoSeleccionado);
        } catch (Exception e) {
            String stackError = new String();
            for (StackTraceElement stackTrace : e.getStackTrace()) {
                stackError = stackError.concat(stackTrace.toString()
                        + "\n");
            }
            procesarValidacionIncorrecta(redFisica,
                    conjuntoRedesVirtuales.getNombre(),
                    algoritmoSeleccionado, null,
                    "Ha ocurrido un error en la ejecucion de "
                    + "su algoritmo, favor "
                    + "verifique\n".concat(e.toString() + "\n"
                            + stackError),
                    (System.nanoTime() - tiempoInicial));
            return null;
        }
        long tiempoFinal = System.nanoTime();
        for (VirtualNetwork redVirtual : conjuntoRedesVirtuales.
                getConjuntoRedes()) {
            if (redVirtual.isMapeado()) {
                try {
                    realizarValidacion((SustrateNetwork) redFisica.clone(),
                            redVirtual);
                    actualizarRedFisica(redFisica, redVirtual);
                } catch (Exception e) {
                    /* Imprimir reporte de error, estado actual de la 
                     * red fisica y la red virtual que se quiso mapear.
                     */
                    procesarValidacionIncorrecta(redFisica,
                            conjuntoRedesVirtuales.getNombre(),
                            algoritmoSeleccionado, redVirtual,
                            e.getMessage(), tiempoFinal - tiempoInicial);
                    return null;
                }
            } else {
                actualizarRedVirtual(redVirtual);
            }
            System.gc();
        }
        long finSimulador = System.nanoTime();
        return ejecutarMetricas(redFisica, conjuntoRedesVirtuales,
                metricasSeleccionadas, tiempoFinal - tiempoInicial,
                algoritmoSeleccionado, finSimulador - inicioSimulador);
    }

    /**
     * Metodo que ejecuta la prueba dinamica.
     *
     * @param redFisica red fisica generada.
     * @param conjuntoRedesVirtuales conjunto de redes virtuales generado.
     * @param algoritmoSeleccionado cadena que almacena el algoritmo
     * seleccionado por el usuario.
     * @param metricasSeleccionadas lista de metricas seleccionadas por el
     * usuario.
     * @return nombre del archivo generado o null.
     */
    public static String ejecutarPruebaDinamica(SustrateNetwork redFisica,
            VirtualNetworkList conjuntoRedesVirtuales,
            String algoritmoSeleccionado,
            ArrayList<String> metricasSeleccionadas) {
        long tiempoTotal = 0, inicioSimulador = System.nanoTime();
        int posicion = 0;
        ArrayList<VirtualNetwork> requerimientosActuales = new ArrayList<>();
        /*Primero ordenamos los requerimientos de redes virtuales generados de 
         *menor a mayor en tiempo de entrada.
         */
        Collections.sort(conjuntoRedesVirtuales.getConjuntoRedes(),
                new Comparator<VirtualNetwork>() {
            @Override
            public int compare(VirtualNetwork o1, VirtualNetwork o2) {
                return new Integer(o1.getTiempoInicial()).
                        compareTo(o2.getTiempoInicial());
            }
        });
        for (int ciclo = 1; ciclo <= conjuntoRedesVirtuales.getCiclos();
                ciclo++) {
            /*
             * Primero se verifica si hay requerimientos que no han podido 
             * ser mapeados y se sacan
             */
            if (requerimientosActuales.size() > 0) {
                int contador = 0;
                do {
                    if (requerimientosActuales.get(contador).isMapeado()) {
                        contador++;
                    } else {
                        requerimientosActuales.remove(contador);
                    }
                } while (contador < requerimientosActuales.size());
            }
            /*Aqui se sacan requerimientos virtuales si ya fenecieron*/
            if (requerimientosActuales.size() > 0) {
                Collections.sort(requerimientosActuales,
                        new Comparator<VirtualNetwork>() {
                    @Override
                    public int compare(VirtualNetwork o1, VirtualNetwork o2) {
                        return new Integer(o1.getTiempoFinal()).
                                compareTo(o2.getTiempoFinal());
                    }
                });
                while (requerimientosActuales.size() > 0
                        && requerimientosActuales.get(0).
                                getTiempoFinal() < ciclo) {
                    if (requerimientosActuales.get(0).isMapeado()) {
                        liberarRecursos(requerimientosActuales.get(0));
                    }
                    requerimientosActuales.remove(0);
                }
            }
            /*Aqui se agregan las redes que tienen que ingresar al sistema*/
            while (posicion < conjuntoRedesVirtuales.getConjuntoRedes().size()
                    && conjuntoRedesVirtuales.getConjuntoRedes().
                            get(posicion).getTiempoInicial() == ciclo) {
                requerimientosActuales.add(conjuntoRedesVirtuales.
                        getConjuntoRedes().get(posicion++));
            }
            /*Por cada VNR, se verifica si la misma ya se encuentra mapeada o 
             *no, si no esta mapeada, se llama al algoritmo de mapeo 
             *seleccionado brindando informacion de capacidad de CPU de los 
             *nodos y posiciones libres de la VNR, donde el usuario que crea el 
             *algoritmo debe devolver la informacion de mapeo del mismno y 
             *luego se procede a la verificacion del mapeo.
             */
            ArrayList<VirtualNetwork> red = new ArrayList<>();
            for (VirtualNetwork redVirtual : requerimientosActuales) {
                if (!redVirtual.isMapeado()) {
                    red.add(redVirtual);
                    long tiempoInicial = System.nanoTime();
                    try {
                        ejecutarAlgoritmoSeleccionado((SustrateNetwork) redFisica.clone(),
                                red, algoritmoSeleccionado);
                    } catch (Exception e) {
                        String stackError = new String();
                        for (StackTraceElement stackTrace : e.getStackTrace()) {
                            stackError = stackError.concat(stackTrace.toString() + "\n");
                        }
                        procesarValidacionIncorrecta(redFisica,
                                conjuntoRedesVirtuales.getNombre(),
                                algoritmoSeleccionado, redVirtual,
                                "Ha ocurrido un error en la ejecucion de "
                                + "su algoritmo, favor "
                                + "verifique\n".concat(e.toString() + "\n"
                                        + stackError),
                                tiempoTotal);
                        return null;
                    }
                    long tiempoFinal = System.nanoTime();
                    red.remove(0);
                    tiempoTotal += tiempoFinal - tiempoInicial;
                    if (redVirtual.isMapeado()) {
                        try {
                            realizarValidacion((SustrateNetwork) redFisica.clone(), redVirtual);
                            actualizarRedFisica(redFisica, redVirtual);
                            redVirtual.setTiempoIngresado(ciclo);
                        } catch (Exception e) {
                            /* Imprimir reporte de error, estado actual de la 
                             * red fisica y la red virtual que se quiso mapear.
                             */
                            procesarValidacionIncorrecta(redFisica,
                                    conjuntoRedesVirtuales.getNombre(),
                                    algoritmoSeleccionado, redVirtual,
                                    e.getMessage(), tiempoTotal);
                            return null;
                        }
                    } else {
                        actualizarRedVirtual(redVirtual);
                    }
                    System.gc();
                }
            }
        }
        long finSimulador = System.nanoTime();
        return ejecutarMetricas(redFisica, conjuntoRedesVirtuales,
                metricasSeleccionadas, tiempoTotal,
                algoritmoSeleccionado, finSimulador - inicioSimulador);
    }

    /**
     * Metodo que devuelve la lista de algoritmos disponibles para el tipo de
     * prueba seleccionado.
     *
     * @param estatico indicar si pasa los algoritmos estaticos o dinamicos.
     * @return lista con los algoritmos registrados en el simulador.
     */
    public static ArrayList<String> listaDeAlgoritmosDisponibles(boolean estatico) {
        ArrayList<String> lista = new ArrayList<>();
        /*Cuando quieren agregar un algoritmo que han hecho para el tipo de 
         *problema realizado, agregar aqui por favor el nombre del mismo, es 
         *decir, si quiere que en la lista de algorimos a seleccionar quiere 
         *que aparezca "StaticACO", ingresar este nombre en la lista que 
         *corresponde
         */
        if (estatico) {
            /*Aqui agregar si es resolucion estatica*/
            lista.add("Voraz Est\u00e1tico");
        } else {
            /*Aqui agregar si es resolucion dinamica*/
            lista.add("Voraz Din\u00e1mico");
        }
        return lista;
    }

    /**
     * Metodo que devuelve la lista de metricas disponibles.
     *
     * @return lista de metricas disponibles.
     */
    public static ArrayList<String> listaDeMetricasDisponibles() {
        ArrayList<String> metricas = new ArrayList<>();
        metricas.add("Tasa de Rechazo");
        metricas.add("Utilizaci\u00f3n promedio de enlace");
        metricas.add("Costo del Embedding");
        metricas.add("Revenue");
        metricas.add("Relaci\u00f3n Revenue - Costo");
        metricas.add("VONE N.P.");
        return metricas;
    }

    /**
     * Metodo que ejecuta el algoritmo seleccionado.
     *
     * @param redFisica red fisica sobre la cual se va a montar las redes
     * virtuales.
     * @param conjuntoRedes conjunto de redes virtuales que seran montadas.
     * @param algoritmoSeleccionado nombre del algoritmo seleccionado a se
     * ejecutado.
     * @throws java.lang.Exception
     */
    private static void ejecutarAlgoritmoSeleccionado(SustrateNetwork redFisica,
            ArrayList<VirtualNetwork> conjuntoRedes, String algoritmoSeleccionado)
            throws Exception {
        /*Aqui tienen que agregar un nuevo case para sus respectivos 
         *algoritmos - utlizando el ejemplo mencionado en el comentario interior 
         *del metodo listaDeAlgoritmosDisponibles, si usted cargo "StaticACO" 
         *en la lista de algoritmos disponibles, sea estatico o dinamico, debe 
         *agregar debajo de todos los case existentes, un case en el cual tenga 
         *como opcion el nombre que usted ha cargado, es decir, case 
         *"StaticACO": y luego colocar abajo la llamada a su metodo (incluir si 
         *o si a redFisica y conjuntoRedes en sus llamadas, ya que los mismos 
         *son los que contienen los datos que luego se utilizaran para correr 
        *las metricas seleccionadas).
         */
        try {
            switch (algoritmoSeleccionado) {
                case "Voraz Est\u00e1tico":
                    Voraz.vorazEstatico(redFisica, conjuntoRedes);
                    break;
                case "Voraz Din\u00e1mico":
                    conjuntoRedes.get(0).setBanderaBloqueo(
                            Voraz.vorazDinamico(redFisica,
                                    conjuntoRedes.get(0)));
                    break;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Metodo que ejecutara las metricas seleccionadas por el usuario y lo
     * registrara en un archivo.
     */
    private static String ejecutarMetricas(SustrateNetwork redFisica,
            VirtualNetworkList conjuntoRedes,
            ArrayList<String> metricasSeleccionadas, long tiempoAlgoritmo,
            String algoritmoEjecutado, long tiempoSimulador) {
        String nombreArchivo = "vone_results";
        String nombreRetornado;
        File directorio = new File(nombreArchivo);
        if (!directorio.exists()) {
            try {
                directorio.mkdir();
            } catch (Exception e) {
                System.out.println("Error al generar el directorio");
                e.printStackTrace();
            }
        }
        if (conjuntoRedes.isEsEstatico()) {
            nombreArchivo = nombreArchivo.concat("/VSResult_"
                    + "static_");
            nombreRetornado = "static_";
        } else {
            nombreArchivo = nombreArchivo.concat("/VSResult_"
                    + "dinamic_");
            nombreRetornado = "dinamic_";
        }
        Date fecha = Calendar.getInstance().getTime();
        nombreArchivo = nombreArchivo.
                concat(new SimpleDateFormat("yyyyMMdd.hhmmss").format(fecha)).
                concat(".txt");
        nombreRetornado = nombreRetornado.
                concat(new SimpleDateFormat("yyyyMMdd.hhmmss").format(fecha)).
                concat(".txt");
        directorio = new File(nombreArchivo);
        System.gc();
        try {
            long segundos, minutos = 0, horas = 0;
            segundos = (tiempoSimulador - tiempoSimulador % 1000000000)
                    / 1000000000;
            if (segundos >= 3600) {
                horas = (segundos - segundos % 3600) / 3600;
                segundos -= horas * 3600;
            }
            if (segundos >= 60) {
                minutos = (segundos - segundos % 60) / 60;
                segundos -= minutos * 60;
            }
            long segundosAlgoritmo = 0, minutosAlgortimo = 0,
                    horasAlgoritmo = 0;
            segundosAlgoritmo = (tiempoAlgoritmo - tiempoAlgoritmo % 1000000000)
                    / 1000000000;
            if (segundosAlgoritmo >= 3600) {
                horasAlgoritmo = (segundosAlgoritmo - segundosAlgoritmo % 3600)
                        / 3600;
                segundosAlgoritmo -= horasAlgoritmo * 3600;
            }
            if (segundosAlgoritmo >= 60) {
                minutosAlgortimo = (segundosAlgoritmo - segundosAlgoritmo % 60) / 60;
                segundosAlgoritmo -= minutosAlgortimo * 60;
            }
            directorio.createNewFile();
            BufferedWriter writer
                    = new BufferedWriter(new FileWriter(directorio));
            writer.write("\t\t\t\tVONE Simulator\nFecha y hora de la prueba: "
                    + new SimpleDateFormat("dd/MM/yyyy").format(fecha)
                    + " " + new SimpleDateFormat("HH:mm").format(fecha)
                    + " hs.\nAlgoritmo ejecutado: " + algoritmoEjecutado
                    + "\nTiempo de ejecución del "
                    + "algoritmo: " + tiempoAlgoritmo + " ns - "
                    + horasAlgoritmo + ":" + minutosAlgortimo
                    + ":" + segundosAlgoritmo + " hs.\n"
                    + "\nTiempo de ejecución "
                    + "del simulador: " + tiempoSimulador + " ns - "
                    + horas + ":" + minutos + ":" + segundos + " hs.\n");
            writer.write("-----------\nResultados de Metricas Seleccionadas"
                    + "\n-----------\n");
            for (String metrica : metricasSeleccionadas) {
                ejecutarMetrica(redFisica, conjuntoRedes, metrica, writer);
            }
            writer.write("\n===\nFin del Reporte\n===\n\n\nA modo de "
                    + "informacion, se imprimer los datos de la red fisica"
                    + " y el conjunto de redes virtuales con sus respectivos "
                    + "mapeos (en caso de haberse mapeado).\n");
            writer.write("Red Fisica utilizada: " + redFisica.getNombre()
                    + "\n");
            writer.write(redFisica.toString() + "\n\n");
            writer.write(conjuntoRedes.toString());
            writer.flush();
            writer.close();
        } catch (Exception e) {
            System.out.println("Error al crear el archivo");
            e.printStackTrace();
        }
        return nombreRetornado;
    }

    /**
     * Metodo para procesar e imprimir el reporte de error de validacion del
     * algoritmo de mapeo.
     *
     * @param redFisica informacion de la red fisica utilizada.
     * @param conjuntoRedesSeleccionado nombre del conjunto de redes al que
     * pertenece la red que ha lanzado el error.
     * @param algoritmoEjecutado
     * @param redVirtual red virtual que se ha mepeado de manera incorrecta.
     * @param mensajeError mensaje de error a imprimir en el reporte de error.
     */
    private static void procesarValidacionIncorrecta(SustrateNetwork redFisica,
            String conjuntoRedesSeleccionado, String algoritmoEjecutado,
            VirtualNetwork redVirtual, String mensajeError,
            long tiempoEjecutado) {
        String nombreArchivo = "vone_error_reports";
        File directorio = new File(nombreArchivo);
        if (!directorio.exists()) {
            try {
                directorio.mkdir();
            } catch (Exception e) {
                System.out.println("Error al generar el directorio");
                e.printStackTrace();
            }
        }
        nombreArchivo = nombreArchivo.concat("/VSErrorReport_");
        Date fecha = Calendar.getInstance().getTime();
        nombreArchivo = nombreArchivo.
                concat(new SimpleDateFormat("yyyyMMdd.hhmmss").format(fecha)).
                concat(".txt");
        directorio = new File(nombreArchivo);
        System.gc();
        try {
            directorio.createNewFile();
            try {
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(directorio))) {
                    writer.write("\t\t\t\tVONE Simulator\nFecha y hora de "
                            + "la prueba: "
                            + new SimpleDateFormat("dd/MM/yyyy").format(fecha)
                            + " " + new SimpleDateFormat("KK:mm").format(fecha)
                            + " hs.\nAlgoritmo ejecutado: " + algoritmoEjecutado
                            + "\nTiempo de ejecucion hasta el momento del error: "
                            + tiempoEjecutado + " ns.\n");
                    writer.write("Mensaje de error: " + mensajeError + "\n");

                    writer.write("\n===\nFin del Reporte\n===\n\n\nA modo de "
                            + "informacion, se imprimer los datos de las red fisica"
                            + " y la ultima red virtual que causo el error.\n");
                    writer.write("Red Fisica utilizada: " + redFisica.getNombre()
                            + "\n");
                    writer.write(redFisica.toString());
                    writer.write("Conjunto de redes utilizado: "
                            + conjuntoRedesSeleccionado);
                    if (redVirtual != null) {
                        writer.write("Red Virtual que ha causado el error: "
                                + redVirtual.getNombre() + "\n");
                        writer.write(redVirtual.toString());
                    }
                    writer.flush();
                }

            } catch (IOException e) {
                System.out.println("Error al crear el archivo");
                e.printStackTrace();
            }
        } catch (Exception e) {
        }
    }

    /**
     * Metodo que ejecuta la metrica que se pasa como parametro.
     *
     * @param redFisica objeto con la informacion de la red fisica.
     * @param conjuntoRedes objeto con la informacion del estado final de los
     * requerimientos de redes virtuales.
     * @param metrica metrica seleccionada.
     * @param writer objeto para escribir
     * @throws java.lang.IOException
     */
    private static void ejecutarMetrica(SustrateNetwork redFisica,
            VirtualNetworkList conjuntoRedes, String metrica,
            BufferedWriter writer) throws IOException {
        switch (metrica) {
            case "Utilizaci\u00f3n promedio de enlace":
                double promedioSolicitado = 0.0;
                int cantidadFSUtilizado = 0;
                for (VirtualNetwork redVirtual : conjuntoRedes.
                        getConjuntoRedes()) {
                    if (redVirtual.isMapeado()) {
                        int fsUtilizado = 0;
                        for (VirtualEdge enlaceVirtual : redVirtual.
                                getEnlacesVirtuales()) {
                            fsUtilizado += enlaceVirtual.getCantidadFS()
                                    * enlaceVirtual.getEnlaceFisico().size();
                        }
                        if (conjuntoRedes.isEsEstatico()) {
                            cantidadFSUtilizado += fsUtilizado;
                        } else {
                            cantidadFSUtilizado += fsUtilizado * (redVirtual.
                                    getTiempoFinal() - redVirtual.
                                            getTiempoIngresado() + 1);
                        }
                    }
                }
                if(conjuntoRedes.isEsEstatico()){
                    promedioSolicitado = (double) cantidadFSUtilizado * 100.00
                        / ((double) (redFisica.getCantidadFS() * redFisica.
                        getNroEnlaces()));
                } else {
                    promedioSolicitado = (double) cantidadFSUtilizado * 100.00
                        / ((double) (redFisica.getCantidadFS() * redFisica.
                        getNroEnlaces() * conjuntoRedes.getCiclos()));
                }
                writer.write(" - El promedio de utilizaci\u00f3n por enlace "
                        + "es de " + new DecimalFormat("#0").format(
                                promedioSolicitado) + " %.\n");
                break;
            case "Tasa de Rechazo":
                int cantidadBloqueados = 0,
                 cantidadBloqueadosPorNodo = 0,
                 cantidadBloqueadosPorEnlace = 0;
                for (VirtualNetwork redVirtual : conjuntoRedes.
                        getConjuntoRedes()) {
                    if (!redVirtual.isMapeado()) {
                        cantidadBloqueados++;
                        if (redVirtual.getBanderaBloqueo() == 1) {
                            cantidadBloqueadosPorNodo++;
                        } else {
                            cantidadBloqueadosPorEnlace++;
                        }
                    }
                }
                writer.write(" - La tasa de rechazo de la ejecuci\u00f3n realizada "
                        + "es de: " + new DecimalFormat("#0.00").format(
                                ((double) cantidadBloqueados
                                / (double) conjuntoRedes.getConjuntoRedes().
                                        size()) * 100.00)
                        + " %\n");
                if (cantidadBloqueados > 0) {
                    writer.write("\t* El porcentaje de rechazos por capacidad de "
                            + "CPU en los nodos es de: "
                            + new DecimalFormat("#0.00").format(
                                    ((double) cantidadBloqueadosPorNodo
                                    / (double) cantidadBloqueados) * 100.00)
                            + " %\n");
                    writer.write("\t* El porcentaje de rechazos por capacidad de "
                            + "enlace es de: " + new DecimalFormat("#0.00").format(
                                    ((double) cantidadBloqueadosPorEnlace
                                    / (double) cantidadBloqueados) * 100.00)
                            + " %\n");
                }
                break;
            case "Costo del Embedding":
                writer.write(" - El costo de asignaci\u00f3n de todos los "
                        + "requerimientos de red fue "
                        + "de " + costoEmbedding(conjuntoRedes)
                        + " unidades monetarias.\n");
                break;
            case "Revenue":
                writer.write(" - El revenue de asignaci\u00f3n de todos los "
                        + "requerimientos de red fue "
                        + "de " + revenueEmbedding(conjuntoRedes)
                        + " unidades monetarias.\n");
                break;
            case "Relaci\u00f3n Revenue - Costo":
                int costo = costoEmbedding(conjuntoRedes);
                int revenue = revenueEmbedding(conjuntoRedes);
                if (revenue > 0) {
                    double ratio = (double) revenue / (double) costo;
                    writer.write(" - La relaci\u00f3n revenue - costo es de "
                            + new DecimalFormat("#0.00").format(ratio) + "\n");
                } else {
                    writer.write(" - La relaci\u00f3n revenue - costo no es "
                            + "posible hallarla (ning\u00fan requerimiento "
                            + "de red virtual fue mapeado).\n");
                }
                break;
            case "VONE N.P.":
                int costoFisico = costoEmbedding(conjuntoRedes);
                int revenueVirtual = revenueEmbedding(conjuntoRedes);
                int revenueTotal = revenueTotal(conjuntoRedes);
                double voneNP = ((double) (revenueVirtual - costoFisico))
                        / (double) (revenueTotal);
                writer.write(" - El resultado de la metrica VONE N.P. "
                        + "seleccionada es de "
                        + new DecimalFormat("#0.00").format(voneNP) + "\n");
                break;
            /*Tiempo de ejecución, Tasa de rechazo, Utilización de CPU, Utilización de Enlace*/
        }
    }

    /**
     * Metodo para hallar el costo del embedding del conjunto de requerimientos
     * de redes virtuales utilizado (los que han sido aceptados).
     *
     * @param redesVirtuales objeto con los datos de todos los requerimientos de
     * red.
     * @return costo del embedding del conjunto.
     */
    private static int costoEmbedding(VirtualNetworkList redesVirtuales) {
        int costoEmbedding = 0;
        for (VirtualNetwork redVirtual : redesVirtuales.getConjuntoRedes()) {
            if (redVirtual.isMapeado()) {
                int cpuUtilizado = 0;
                int fsUtilizado = 0;
                for (VirtualNode nodoVirtual : redVirtual.getNodosVirtuales()) {
                    cpuUtilizado += nodoVirtual.getCapacidadCPU();
                }
                for (VirtualEdge enlaceVirtual : redVirtual.
                        getEnlacesVirtuales()) {
                    fsUtilizado += enlaceVirtual.getCantidadFS() * enlaceVirtual.
                            getEnlaceFisico().size();
                }
                if (redesVirtuales.isEsEstatico()) {
                    costoEmbedding += (cpuUtilizado + fsUtilizado);
                } else {
                    costoEmbedding += (cpuUtilizado + fsUtilizado) * (redVirtual.
                            getTiempoFinal() - redVirtual.getTiempoIngresado()
                            + 1);
                }
            }
        }

        return costoEmbedding;
    }

    /**
     * Metodo para hallar el revenue de la asignacion de un conjunto de
     * requerimientos de red virtual (los que han sido aceptados).
     *
     * @param redesVirtuales objeto con los datos de todos los requerimientos de
     * red.
     * @return revenue del embedding del conjunto.
     */
    private static int revenueEmbedding(VirtualNetworkList redesVirtuales) {
        int revenueEmbedding = 0;
        for (VirtualNetwork redVirtual : redesVirtuales.getConjuntoRedes()) {
            if (redVirtual.isMapeado()) {
                int cpuUtilizado = 0;
                int fsUtilizado = 0;
                for (VirtualNode nodoVirtual : redVirtual.getNodosVirtuales()) {
                    cpuUtilizado += nodoVirtual.getCapacidadCPU();
                }
                for (VirtualEdge enlaceVirtual : redVirtual.
                        getEnlacesVirtuales()) {
                    fsUtilizado += enlaceVirtual.getCantidadFS();
                }
                if (redesVirtuales.isEsEstatico()) {
                    revenueEmbedding += 3 * (cpuUtilizado + fsUtilizado);
                } else {
                    revenueEmbedding += 3 * (cpuUtilizado + fsUtilizado)
                            * (redVirtual.getTiempoFinal()
                            - redVirtual.getTiempoIngresado() + 1);
                }
            }
        }
        return revenueEmbedding;
    }

    /**
     * Metodo para hallar el revenue de la asignacion de un conjunto de
     * requerimientos de red virtual (los que han sido aceptados y tambien los
     * bloqueados).
     *
     * @param redesVirtuales objeto con los datos de todos los requerimientos de
     * red.
     * @return revenue del embedding del conjunto.
     */
    private static int revenueTotal(VirtualNetworkList redesVirtuales) {
        int revenueTotal = 0;
        for (VirtualNetwork redVirtual : redesVirtuales.getConjuntoRedes()) {
            int cpuUtilizado = 0;
            int fsUtilizado = 0;
            for (VirtualNode nodoVirtual : redVirtual.getNodosVirtuales()) {
                cpuUtilizado += nodoVirtual.getCapacidadCPU();
            }
            for (VirtualEdge enlaceVirtual : redVirtual.
                    getEnlacesVirtuales()) {
                fsUtilizado += enlaceVirtual.getCantidadFS();
            }
            if (redesVirtuales.isEsEstatico()) {
                revenueTotal += 3 * (cpuUtilizado + fsUtilizado);
            } else {
                revenueTotal += 3 * (cpuUtilizado + fsUtilizado) * (redVirtual.
                        getTiempoFinal() - redVirtual.getTiempoIngresado() + 1);
            }
        }
        return revenueTotal;
    }

    /**
     * Metodo que retorna la capacidad actual del nodo.
     *
     * @param nodo
     * @return capacidad disponible de CPU.
     */
    private static int capacidadActual(SustrateNode nodo) {
        return nodo.getCapacidadCPU() - nodo.getCPU().size();
    }

    /**
     * Metodo que retorna si el nodo cuenta con capacidad suficiente para
     * albergar al nodo virtual que quiere ser seteado.
     *
     * @param capacidadNodoVirtual capacidad de CPU del nodo que quiere ser
     * seteado alli.
     * @param nodo
     * @return true si es posible setear.
     */
    private static boolean tieneCapacidad(int capacidadNodoVirtual,
            SustrateNode nodo) {
        return capacidadNodoVirtual <= nodo.capacidadActual();
    }

    /**
     * Metodo para asignar capacidad de CPU en el nodo fisico (se asume que ya
     * se ha ejecutado la verificacion del nodo).
     *
     * @param nodo nodo fisico al cual se asignara capacidad de CPU.
     * @param capacidad capacidad a ser asignada.
     */
    private static void asignarRecursoCPU(SustrateNode nodo, int capacidad) {
        for (int i = 0; i < capacidad; i++) {
            nodo.getCPU().add(i);
        }
    }

    /**
     * Metodo para asignar capacidad de CPU en el nodo fisico (se asume que ya
     * se ha ejecutado la verificacion del nodo).
     *
     * @param nodo nodo fisica que se desasignara capacidad de CPU.
     * @param capacidad capacidad a ser desasignada.
     */
    private static void desAsignarRecursoCPU(SustrateNode nodo, int capacidad) {
        for (int i = 0; i < capacidad; i++) {
            nodo.getCPU().remove(0);
        }
    }

    /**
     * Metodo que verifica la disponibilidad del enlace.
     *
     * @param posicionInicial posicion inicial a verificar.
     * @param cantidadNecesaria cantidad de frequency slots que son necesarios
     * almacenar en el enlace.
     * @param enlace informacion del enlace al cual se realizara la
     * verificacion.
     * @return true si tiene disponibilidad en las posicion solicitada.
     */
    public static boolean estaLibre(int posicionInicial, int cantidadNecesaria,
            SustrateEdge enlace) {
        for (int i = posicionInicial; i < posicionInicial + cantidadNecesaria;
                i++) {
            if (enlace.devolverEstadoFS(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Metodo para asignar los FS en los enlaces virtuales (se asume que ya se
     * ha ejecutado la verificacion del enlace).
     *
     * @param enlace lista con todos los enlaces que contendran al enlace
     * virtual.
     * @param posicionInicial posicion inicial del ligthpath a crear.
     * @param cantidadFSNecesaria cantidad de frequency slots a contener el
     * ligthpath.
     */
    public static void asignarEnlacesVirtualesAFisicos(SustrateEdge enlace,
            int posicionInicial, int cantidadFSNecesaria) {
        for (int i = posicionInicial; i < posicionInicial
                + cantidadFSNecesaria; i++) {
            enlace.asignarEstadoFS(true, i);
        }
    }

    /**
     * Metodo que desasigna la capacidad de CPU y enlace ocupados por los
     * requerimientos virtuales que han fenecido su tiempo de vida dentro del
     * sistema.
     *
     * @param redFisica red fisica a la cual se le va a liberar recursos.
     * @param redesASacar conjunto de redes virtuales que van a ser desasignadas
     * de la red fisica.
     */
    private static void liberarRecursos(VirtualNetwork redVirtual) {
        redVirtual.getNodosVirtuales().forEach((nodo) -> {
            nodo.getNodoFisico().desAsignarRecursoCPU(nodo.getCapacidadCPU());
            //desAsignarRecursoCPU(nodo.getNodoFisico(), nodo.getCapacidadCPU());
            //int capacidadActual = nodo.getNodoFisico().getCPU().size();
        });
        redVirtual.getEnlacesVirtuales().forEach((enlace) -> {
            enlace.getEnlaceFisico().forEach((enlaceFisico) -> {
                for (int posicion = enlace.getPosicionFisica();
                        posicion < enlace.getPosicionFisica()
                        + enlace.getCantidadFS(); posicion++) {
                    enlaceFisico.asignarEstadoFS(false, posicion);
                }
            });
        });
    }

    /**
     * Metodo que realiza la validacion y asignacion de una red virtual sobre la
     * red fisica que se esta utilizando.
     *
     * @param redFisica objeto con informacion de la red fisica en su estado
     * actual.
     * @param redVirtual objeto que contiene informacion de la red virtual y la
     * informacion del mapeo del mismo sobre la red fisica.
     * @throws java.lang.Exception
     */
    private static void realizarValidacion(SustrateNetwork redFisica,
            VirtualNetwork redVirtual) throws Exception {
        /* Primero se verifica si realmente todos los nodos y enlaces están con 
         * estado mapeado y de ser asi, ver si tienen alguna referencia.
         */
        //redVirtual.getNodosVirtuales().forEach((nodoVirtual) -> {
        for (int i = 0; i < redVirtual.getNodosVirtuales().size(); i++) {
            VirtualNode nodoVirtual = redVirtual.getNodosVirtuales().get(i);
            if (nodoVirtual.isMapeado()) {
                if (nodoVirtual.getNodoFisico() == null) {
                    /* Lanzar excepcion de que indica que mapeo 
                     * pero que en realidad no fue asi
                     */
                    throw new Exception("Se ha indicado que el nodo "
                            + "" + nodoVirtual.getNombre() + " de la VNR "
                            + redVirtual.getNombre()
                            + " ha sido mapeado, pero no se incluye la "
                            + "informacion del nodo fisico en el cual se "
                            + "va a mapear.");
                }
                boolean estaCorrectamenteMapeado = false;
                for (SustrateNode nodoFisico : redFisica.getNodosFisicos()) {
                    if (nodoVirtual.getNodoFisico().getIdentificador()
                            == nodoFisico.getIdentificador()) {
                        estaCorrectamenteMapeado = true;
                        break;
                    }
                }
                if (!estaCorrectamenteMapeado) {
                    /* Lanzar excepcion indicando que no esta mapeado
                     * correctamente
                     */
                    throw new Exception("");
                }
            } else {
                /*Lanzar excepicion de que no es cierto que se mapeo*/
                throw new Exception("Indica que la red se encuentra mapeada"
                        + ", pero el nodo " + nodoVirtual.getNombre()
                        + " de la red " + redVirtual.getNombre()
                        + " se encuentra en estado no mapeado. Por favor, "
                        + "verifique si su algoritmo esta cambiando o no el"
                        + " estado del mismo a mapeado");
            }
        }

        for (VirtualEdge enlaceVirtual : redVirtual.getEnlacesVirtuales()) {
            if (enlaceVirtual.isMapeado()) {
                if (!(enlaceVirtual.getEnlaceFisico().size() > 0)) {
                    /* Lanzar excepcion indicando que seteo como mapeado pero 
                     * en realidad no cuenta con ningun enlace fisico.
                     */
                    throw new Exception("El enlace virtual "
                            + enlaceVirtual.getNombre() + " de la red virtual "
                            + redVirtual.getNombre() + " esta indicado que se "
                            + "ha mapeado, pero no tiene ninguna "
                            + "informaci\u00f3n de enlaces fisicos relacionados"
                            + " a dicho enlace virtual.");
                }
            } else {
                /*Lanzar excepcion de que no es cierto que esta mapeada la red*/
                throw new Exception("Se ha indicado qué la red virtual "
                        + redVirtual.getNombre() + " se ha mapeado a la red "
                        + "fisica seleccionada pero el enlace "
                        + enlaceVirtual.getNombre() + " se encuentra en estado "
                        + "no mapeado.");
            }
        }
        /* Aqui comienza la validacion de los nodos.
         * Primero se verifica que dos nodos virtuales no esten mapeados en un 
         * mismo nodo fisico.
         */
        for (int posicionActual = 0; posicionActual < redVirtual.
                getNodosVirtuales().size(); posicionActual++) {
            VirtualNode nodoVirtual = redVirtual.getNodosVirtuales().
                    get(posicionActual);
            if (posicionActual < redVirtual.getNodosVirtuales().size() - 1) {
                for (int i = posicionActual + 1; i < redVirtual.
                        getNodosVirtuales().size(); i++) {
                    if (redVirtual.getNodosVirtuales().get(i).getNodoFisico()
                            == nodoVirtual.getNodoFisico()) {
                        /* Lanzar excepcion indicando que hay mas de un nodo 
                         * virtual de una misma red dentro de uno fisico.
                         */
                        throw new Exception("El nodo virtual "
                                + nodoVirtual.getNombre() + " de la red "
                                + "virtual " + redVirtual.getNombre()
                                + " quiere ser mapeado a un nodo fisico que ya "
                                + "contiene un nodo virtual de la misma red "
                                + "virtual.");
                    }
                }
            }
        }

        /* Ahora se verifica si el nodo fisico seleccionado cuenta con capacidad 
         * para albergar al nodo virtual y lo asigna, si alguno falla, lanza la
         * excepcion correspondiente.
         */
        for (VirtualNode nodoVirtual : redVirtual.getNodosVirtuales()) {
            for (SustrateNode nodoFisico : redFisica.getNodosFisicos()) {
                if (nodoFisico.getIdentificador() == nodoVirtual.
                        getNodoFisico().getIdentificador()) {
                    if (nodoFisico.tieneCapacidad(nodoVirtual.getCapacidadCPU())/*tieneCapacidad(nodoVirtual.getCapacidadCPU(),
                            nodoFisico)*/) {
                        nodoFisico.asignarRecursoCPU(nodoVirtual.getCapacidadCPU());
                        /*asignarRecursoCPU(nodoFisico, nodoVirtual.
                                getCapacidadCPU());*/
                        break;
                    } else {
                        /*Lanzar excepcion indicando que no posee capacidad.*/
                        throw new Exception("El nodo virtual "
                                + nodoVirtual.getNombre() + " de la red "
                                + "virtual " + redVirtual.getNombre()
                                + " no puede ser mapeado al nodo fisico "
                                + "seleccionado, ya que el mismo no cuenta con "
                                + "suficiente capacidad de CPU para poder "
                                + "albergar el nodo virtual mencionado.");
                    }
                }
            }
        }

        /*Aqui comienza todo lo que respecta a los enlaces
         * Verificacion de si es un camino entre los nodos fisicos con 
         * todos los enlaces.
         */
        for (VirtualEdge enlaceVirtual : redVirtual.getEnlacesVirtuales()) {
            boolean encontrado = false;
            int posicionInicial = -1, posicionFinal = -1;
            for (int i = 0; i < enlaceVirtual.getEnlaceFisico().size(); i++) {
                if (enlaceVirtual.getEnlaceFisico().get(i).getNodoUno().
                        getIdentificador()
                        == enlaceVirtual.getNodoUno().getNodoFisico().
                                getIdentificador()
                        || enlaceVirtual.getEnlaceFisico().get(i).getNodoDos().
                                getIdentificador()
                        == enlaceVirtual.getNodoUno().getNodoFisico().
                                getIdentificador()) {
                    posicionInicial = i;
                }
                if (enlaceVirtual.getEnlaceFisico().get(i).getNodoDos().
                        getIdentificador()
                        == enlaceVirtual.getNodoDos().getNodoFisico().
                                getIdentificador()
                        || enlaceVirtual.getEnlaceFisico().get(i).getNodoUno().
                                getIdentificador()
                        == enlaceVirtual.getNodoDos().getNodoFisico().
                                getIdentificador()) {
                    posicionFinal = i;
                }
                if (posicionInicial != -1 && posicionFinal != -1) {
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado) {
                /* Lanzar excepcion que indica que no hay enlaces que conectan 
                 * a los nodos fisicos en donde se mapearon los nodos uno y dos 
                 * del enlace virtual.
                 */
                throw new Exception("El mapeo realizado para el enlace"
                        + enlaceVirtual.getNombre() + " de la red "
                        + "virtual " + redVirtual.getNombre() + " no "
                        + "cuenta con un camino v\u00e1lido entre los "
                        + "nodos f\u00edsicos correspondientes a los "
                        + "nodos virtuales correspondientes a este "
                        + "enlace.");
            }
            int posicionActual = posicionInicial, idNodoActual;
            ArrayList<Integer> camino = new ArrayList<>();
            ArrayList<Integer> nodos = new ArrayList<>();
            camino.add(posicionInicial);
            if (enlaceVirtual.getNodoUno().getNodoFisico().getIdentificador()
                    == enlaceVirtual.getEnlaceFisico().get(posicionInicial).
                            getNodoUno().getIdentificador()) {
                nodos.add(enlaceVirtual.getEnlaceFisico().
                        get(posicionInicial).getNodoUno().getIdentificador());
                idNodoActual = enlaceVirtual.getEnlaceFisico().
                        get(posicionInicial).getNodoDos().getIdentificador();
            } else {
                nodos.add(enlaceVirtual.getEnlaceFisico().
                        get(posicionInicial).getNodoDos().getIdentificador());
                idNodoActual = enlaceVirtual.getEnlaceFisico().
                        get(posicionInicial).getNodoUno().getIdentificador();
            }
            nodos.add(idNodoActual);
            while (posicionActual != posicionFinal) {
                encontrado = false;
                for (int i = 0; i < enlaceVirtual.getEnlaceFisico().size();
                        i++) {
                    if (enlaceVirtual.getEnlaceFisico().get(i).getNodoUno().
                            getIdentificador() == idNodoActual
                            && !camino.contains(i)) {
                        idNodoActual = enlaceVirtual.getEnlaceFisico().get(i).
                                getNodoDos().getIdentificador();
                        if (nodos.contains(idNodoActual)) {
                            /*Lanzar excepcion de que existe un ciclo*/
                            throw new Exception("El mapeo del enlace "
                                    + enlaceVirtual.getNombre()
                                    + " de la red virtual "
                                    + redVirtual.getNombre() + " sobre la red "
                                    + "f\u00edsica seleccionada contiene un "
                                    + "ciclo entre los enlaces f\u00edsicos "
                                    + "seleccionados.");
                        } else {
                            nodos.add(idNodoActual);
                        }
                        encontrado = true;
                        posicionActual = i;
                        break;
                    }
                    if (enlaceVirtual.getEnlaceFisico().get(i).
                            getNodoDos().getIdentificador() == idNodoActual
                            && !camino.contains(i)) {
                        idNodoActual = enlaceVirtual.getEnlaceFisico().get(i).
                                getNodoUno().getIdentificador();
                        if (nodos.contains(idNodoActual)) {
                            /*Lanzar excepcion de que existe un ciclo*/
                            throw new Exception("El mapeo del enlace "
                                    + enlaceVirtual.getNombre()
                                    + " de la red virtual "
                                    + redVirtual.getNombre() + " sobre la red "
                                    + "f\u00edsica seleccionada contiene un "
                                    + "ciclo entre los enlaces f\u00edsicos "
                                    + "seleccionados.");
                        } else {
                            nodos.add(idNodoActual);
                        }
                        encontrado = true;
                        posicionActual = i;
                        break;
                    }
                }
                if (!encontrado) {
                    /* Lanzar excepcion de que indique que el conjunto de 
                     * enlaces no estan interconectados.
                     */
                    throw new Exception("El mapeo del enlace virtual "
                            + enlaceVirtual.getNombre() + " de la red virtual"
                            + redVirtual.getNombre() + " sobre la red "
                            + "f\u00edsica seleccionada "
                            + "no es un camino que interconecta los"
                            + " nodos f\u00edsicos a los cuales se han mapeado "
                            + "los nodos virtuales que se relacionan mediante "
                            + "el  enlace virtual mencionado.");
                }
                if (camino.contains(posicionActual)) {
                    /* Lanzar excepcion que indique que el enlace fisico ya se 
                     * encuentra dentro del sistema.
                     */
                    throw new Exception("El mapeo del enlace virtual "
                            + enlaceVirtual.getNombre() + " de la red virtual"
                            + redVirtual.getNombre() + "sobre la red "
                            + "f\u00edsica contiene dos veces un enlace "
                            + "fisico del mapeo.");
                } else {
                    camino.add(posicionActual);
                }
            }
            if (enlaceVirtual.getEnlaceFisico().size() != camino.size()) {
                /* Lanzar excepcion de que hay mas enlaces dentro del conjunto 
                 * que no forman parte del camino.
                 */
                throw new Exception("El conjunto de enlaces f\u00edsicos "
                        + "asociados al enlace virtual "
                        + enlaceVirtual.getNombre() + " de la red virtual "
                        + redVirtual.getNombre() + " es diferente a la cantidad"
                        + " de enlaces necesarios para crear un camino entre "
                        + "los nodos f\u00edsicos a los cuales se han mapeado "
                        + "los nodos virtuales que se relacionan mediante "
                        + "el  enlace virtual mencionado.");
            }
            if (enlaceVirtual.getPosicionFisica() < 0) {
                /* 
                 * Lanzar excepcion de que el la posicion inicial no es valida.
                 */
                throw new Exception("El rango asignado para la asignaci\u00f3n "
                        + "del enlace virtual " + enlaceVirtual.getNombre()
                        + " de la red virtual " + redVirtual.getNombre()
                        + " sobre los enlaces f\u00edsicos del conjunto que "
                        + "representa al mismo no es v\u00e1lido (ha ingresado "
                        + "que el mismo comience en el frequency slot "
                        + enlaceVirtual.getPosicionFisica() + " y el mismo debe"
                        + " iniciar en cero o mayor a cero)");
            }
            if ((enlaceVirtual.getPosicionFisica() + enlaceVirtual.
                    getCantidadFS() - 1) > redFisica.getCantidadFS()) {
                /* 
                 * Lanzar excepcion de que el rango especificado no es valido
                 */
                throw new Exception("El rango asignado para la asignaci\u00f3n "
                        + "del enlace virtual " + enlaceVirtual.getNombre()
                        + " de la red virtual " + redVirtual.getNombre()
                        + " sobre los enlaces f\u00edsicos del conjunto que "
                        + "representa al mismo no es v\u00e1lido (ha ingresado "
                        + "que el mismo comience en el frequency slot "
                        + enlaceVirtual.getPosicionFisica() + " y termine en "
                        + "el frequency slot " + (enlaceVirtual.
                                getPosicionFisica() + enlaceVirtual.
                                getCantidadFS() - 1) + ", siendo que los "
                        + "enlaces físicos tienen solo hasta " + redFisica.
                                getCantidadFS() + " frequency slots "
                        + "disponibles)");
            }
            for (SustrateEdge enlaceFisicoRelacionado : enlaceVirtual.getEnlaceFisico()) {
                int posicionEnlace = 0;
                for (int i = 0; i < redFisica.getEnlacesFisicos().size(); i++) {
                    if (redFisica.getEnlacesFisicos().get(i).getNombre().
                            equals(enlaceFisicoRelacionado.getNombre())) {
                        posicionEnlace = i;
                        break;
                    }
                }

                if (estaLibre(enlaceVirtual.getPosicionFisica(),
                        enlaceVirtual.getCantidadFS(),
                        redFisica.getEnlacesFisicos().get(posicionEnlace))) {
                    asignarEnlacesVirtualesAFisicos(redFisica
                            .getEnlacesFisicos().get(posicionEnlace),
                            enlaceVirtual.getPosicionFisica(),
                            enlaceVirtual.getCantidadFS());
                } else {
                    /* Lanzar excepcion de que las posiciones de FS ya se 
                     * encuentran ocupadas.
                     */
                    throw new Exception("En el enlace f\u00edsico "
                            + enlaceFisicoRelacionado.getNombre()
                            + " no es posible mapear el camino o lightpath "
                            + "correspondiente al enlace virtual "
                            + enlaceVirtual.getNombre() + " de la red virtual "
                            + redVirtual.getNombre() + "ya que el mismo se "
                            + "encuentra ocupado por otro lightpath mapeado "
                            + "anteriormente.");
                }
            }
        }
    }

    /**
     * Metodo para actualizar el estado de la red fisica y actualizar las
     * referencias de la red virtual a objetos de la red fisica original.
     *
     * @param redFisica red fisica que va a ser actualizada.
     * @param redVirtual red virtual que va a ser actualizada sus referencias a
     * los objetos de la red fisica original.
     */
    private static void actualizarRedFisica(SustrateNetwork redFisica,
            VirtualNetwork redVirtual) {
        /*Primero se realiza la actualizacion para los nodos.*/
        redVirtual.getNodosVirtuales().forEach((nodoVirtual) -> {
            for (int i = 0; i < redFisica.getNodosFisicos().size(); i++) {
                if (redFisica.getNodosFisicos().get(i).getIdentificador()
                        == nodoVirtual.getNodoFisico().getIdentificador()) {
                    asignarRecursoCPU(redFisica.getNodosFisicos().get(i),
                            nodoVirtual.getCapacidadCPU());
                    nodoVirtual.setNodoFisico(redFisica.getNodosFisicos().
                            get(i));
                    break;
                }
            }
        });

        /*Siguiendo la ejecucion se realiza la actualizacion para los enlaces.*/
        redVirtual.getEnlacesVirtuales().forEach((enlaceVirtual) -> {
            int cantidadEnlaces = enlaceVirtual.getEnlaceFisico().size();
            for (int i = 0; i < cantidadEnlaces; i++) {
                for (int j = 0; j < redFisica.getEnlacesFisicos().size(); j++) {
                    if (redFisica.getEnlacesFisicos().get(j).getNombre().
                            equals(enlaceVirtual.getEnlaceFisico().get(i).
                                    getNombre())) {
                        asignarEnlacesVirtualesAFisicos(redFisica.
                                getEnlacesFisicos().get(j),
                                enlaceVirtual.getPosicionFisica(),
                                enlaceVirtual.getCantidadFS());
                        enlaceVirtual.getEnlaceFisico().
                                set(i, redFisica.getEnlacesFisicos().get(j));
                        break;
                    }
                }
            }
        });
    }

    /**
     * Metodo para limpiar los valores una red virtual que no fue mapeada.
     *
     * @param redVirtual red virtual la cual sera limpiada los valores de mapeo
     * que haya quedado.
     */
    private static void actualizarRedVirtual(VirtualNetwork redVirtual) {
        redVirtual.getNodosVirtuales().forEach((nodoVirtual) -> {
            nodoVirtual.setNodoFisico(null);
            nodoVirtual.setMapeado(false);
        });
        redVirtual.getEnlacesVirtuales().forEach((enlaceVirtual) -> {
            enlaceVirtual.setEnlaceFisico(new ArrayList<>());
            enlaceVirtual.setMapeado(false);
        });
    }
}
